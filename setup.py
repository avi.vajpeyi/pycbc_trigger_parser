"""
Setup file for pycbc_trigger_parser.

developers: install with `python setup.py develop` (or `pip install -e .`)
users: install with `python setup.py install`
"""

import sys

from setuptools import setup

# EXE NAMES
PYCBC_BANK_EXTRACTION = "pycbc_bank_extraction"
PYCBC_TRIGGER_EXTRACTION = "pycbc_trigger_extraction"
PYCBC_FILTER_TRIGGERS = "pycbc_filter_triggers"
PYCBC_GET_TRIGGER_DATA = "pycbc_get_trigger_data"
PYCBC_TRIGGER_PLOT = "pycbc_trigger_plot"
PYCBC_CATALOGUE_ADDER = "pycbc_catalogue_adder"

# MODULES
MAIN_MODULE = "pycbc_trigger_parser"
CLI_MODULE = f"{MAIN_MODULE}.cli"
PLOTTING_MODULE = f"{MAIN_MODULE}.plotting"
DATA_MODULE = f"{MAIN_MODULE}.data"

# FUNCTIONS
GET_BANK = "main_get_bank"
GET_RAW_TRIGGERS = "main_get_raw_triggers"
FILTER_TRIGGERS = "main_filter_triggers"
EXTRACT_TRIGGER_META_DATA = "main_extract_trigger_meta_data"
PLOT_TRIGGERS = "main_plot_triggers"
GET_CATALOGUE = "main_add_catalogue_triggers"

# check that python version is 3.5 or above
python_version = sys.version_info
print("Running Python version %s.%s.%s" % python_version[:3])
if python_version < (3, 5):
    sys.exit("Python < 3.5 is not supported, aborting setup")
print("Confirmed Python version 3.5.0 or above")
setup(
    name="pycbc_trigger_parser",
    python_requires=">3.5.0",
    version="0.1",
    description="A module to help extract pyCBC trigger information.",
    author="Avi Vajpeyi",
    author_email="avi.vajpeyi@gmail.com",
    packages=[MAIN_MODULE, CLI_MODULE, PLOTTING_MODULE, DATA_MODULE],
    package_dir={MAIN_MODULE: MAIN_MODULE},
    package_data={MAIN_MODULE: ["data/*.csv"]},
    install_requires=[
        "pandas>=0.25.0",
        "black",
        "flake8",
        "tqdm",
        "bilby>=0.5.4",
        "bilby_report @ git+https://git.ligo.org/avi.vajpeyi/bilby_report.git",
        "matplotlib>=3.1.1",
        "plotly>=4.0.0",
        "h5py>=2.10.0",
        "pytest-cov",
        "coverage-badge",
        "gwpy>=0.15.0",
        "python-ligo-lw>=1.5.3",
        "astroML>=0.4.1",
        "mock>=3.0.5",
        "lalsuite>=6.59",
    ],
    entry_points={
        "console_scripts": [
            f"{PYCBC_BANK_EXTRACTION}={CLI_MODULE}:{GET_BANK}",
            f"{PYCBC_TRIGGER_EXTRACTION}={CLI_MODULE}:{GET_RAW_TRIGGERS}",
            f"{PYCBC_FILTER_TRIGGERS}={CLI_MODULE}:{FILTER_TRIGGERS}",
            f"{PYCBC_GET_TRIGGER_DATA}={CLI_MODULE}:{EXTRACT_TRIGGER_META_DATA}",
            f"{PYCBC_TRIGGER_PLOT}={CLI_MODULE}:{PLOT_TRIGGERS}",
            f"{PYCBC_CATALOGUE_ADDER}={CLI_MODULE}:{GET_CATALOGUE}",
        ]
    },
)
