<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [pyCBC Background triggers](#pycbc-background-triggers)
  - [Structure of O3 Chunk 6's background statmap](#structure-of-o3-chunk-6s-background-statmap)
    - [The different backgrounds](#the-different-backgrounds)
    - [The attributes of the backgrounds](#the-attributes-of-the-backgrounds)
    - [Querying the labels of the detecors](#querying-the-labels-of-the-detecors)
    - [Is there a pyCBC-way to obtain the parameter values of the template used from the](#is-there-a-pycbc-way-to-obtain-the-parameter-values-of-the-template-used-from-the)
    - [Print out the entire structure](#print-out-the-entire-structure)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# pyCBC Background triggers

This repository helps users parse pyCBC trigger files.




## Structure of O3 Chunk 6's background statmap

### The different backgrounds

- `background`
The final set of triggers (after time-sliding), including the events that appear in
coincidence in the non-time slid data.

- `background_exc` (background exclusive of zerolag)
Computes the background after removing all events that appear in coincidence in the
non-time slid data. The reason for this is to prevent the real gravitational-wave
events from showing up in the time-slid background,  although some events may only
be seen in a single detector, and then will appear here.

- `foregound`
The true foreground

- `foreground_hX`
where `X` can be some number -- the hierarchical removal.
Each group stores the bkg removing the X loudest event(s)

*How are the real GW events removed *
We veto (remove) any single-detector triggers occurring at times when
there is a zero-lag coincidence within +/- (something like) 0.2
seconds. This will *not* remove real GW events seen only in a single
detector.

### The attributes of the backgrounds

 * `decimation_factor`
  We can't store all time-slides at all values
of the statistic, it's too many triggers. Therefore we only store a
subset of events at lower values of the statistic. This describes how
that "decimation" is done ... Probably not important in this case.

 * `stat`
 The loudness of the event. Larger stat = more significant.

 * `template_id`
 This is used to identify which template this corresponds to.

 * `timeslide_id`
 Multiply this by the "timeslide-interval" (0.1s
in this case) to get the length of the time slide.
  NOTE: timeslid_time = time + timeslide value

 *  `trigger_id[1,2]`
 This is used to identify which single detector events in the HDF_TRIGGER_MERGE files
  this corresponds to.


 *  `time[1,2]`
 The end time of the events in the two detectors (this is the non-time-slid time).
PyCBC follows the CBC convention of using the trigger's *merger* time
when labelling "times". If you want the start time, you'd have to
identify how long the waveform is (which means choosing a low
frequency cutoff), and subtract that off. We do store low-frequency
cutoffs that the search uses in the bank file, but we do not store
template durations. `NOTE: this is the trigger time`

### Querying the labels of the detecors
In [0]: h5file.attrs['detector_1']
Out[0]: 'H1'

In [1]: h5file.attrs['detector_2']
Out[1]: 'L1'

### Is there a pyCBC-way to obtain the parameter values of the template used from the
`template_id`?

So basically template_id = 2 means the third (as python counts from 0)
entry in the BANK file. Or trigger_id1 = 0 means the first entry in
the HDF_TRIGGER_MERGE file.

There is a PyCBC class to access all of this information:

https://github.com/gwastro/pycbc/blob/master/pycbc/io/hdf.py#L665

something like:


```python
import os
from pycbc.io import ForegroundTriggers
import h5py

ROOT = "/home/tdent/o3/analysis/pycbc/c00/hl/a6_gating_1/output/full_data/"
STAT_MAP = os.path.join(
    ROOT, "H1L1-STATMAP_FULL_DATA_FULL_VETOES-1241724868-760282.hdf"
)
H1_TRIGGER_MERGE = os.path.join(
    ROOT, "H1-HDF_TRIGGER_MERGE_FULL_DATA-1241724868-760282.hdf"
)
L1_TRIGGER_MERGE = os.path.join(
    ROOT, "L1-HDF_TRIGGER_MERGE_FULL_DATA-1241724868-760282.hdf"
)
BANK_FILE = os.path.join(ROOT, "../bank/H1L1-BANK2HDF-1241724868-760282.hdf")
BACKGROUND_TYPE = "background_exc"

background_triggers = ForegroundTriggers(
    coinc_file=STAT_MAP,
    bank_file=BANK_FILE,
    sngl_files=[H1_TRIGGER_MERGE, L1_TRIGGER_MERGE],
    n_loudest=None,
    group=BACKGROUND_TYPE,
)
```


- iFAR (in the statmap)
- template parameters (from the
- GPS start time [H1, L1] (not sure if this is stored...)
- GPS end-time [H1, L1] (statmap's time[1,2])
- timeslide value [H1, L1] (statmap's timeslide_interval * timeslide_id)


### Print out the entire structure
`h5ls -r H1L1-STATMAP_FULL_DATA_FULL_VETOES-1241724868-760282.hdf`
```
/                        Group
/background              Group
/background/decimation_factor Dataset {394711}
/background/ifar         Dataset {394711}
/background/stat         Dataset {394711}
/background/template_id  Dataset {394711}
/background/time1        Dataset {394711}
/background/time2        Dataset {394711}
/background/timeslide_id Dataset {394711}
/background/trigger_id1  Dataset {394711}
/background/trigger_id2  Dataset {394711}
/background_exc          Group
/background_exc/decimation_factor Dataset {382933}
/background_exc/ifar     Dataset {382933}
/background_exc/stat     Dataset {382933}
/background_exc/template_id Dataset {382933}
/background_exc/time1    Dataset {382933}
/background_exc/time2    Dataset {382933}
/background_exc/timeslide_id Dataset {382933}
/background_exc/trigger_id1 Dataset {382933}
/background_exc/trigger_id2 Dataset {382933}
/background_h0           Group
/background_h0/ifar      Dataset {394711}
/background_h0/stat      Dataset {394711}
/background_h0/timeslide_id Dataset {394711}
/background_h1           Group
/background_h1/decimation_factor Dataset {391595}
/background_h1/ifar      Dataset {391595}
/background_h1/stat      Dataset {391595}
/background_h1/template_id Dataset {391595}
/background_h1/time1     Dataset {391595}
/background_h1/time2     Dataset {391595}
/background_h1/timeslide_id Dataset {391595}
/background_h1/trigger_id1 Dataset {391595}
/background_h1/trigger_id2 Dataset {391595}
/background_h2           Group
/background_h2/decimation_factor Dataset {391409}
/background_h2/ifar      Dataset {391409}
/background_h2/stat      Dataset {391409}
/background_h2/template_id Dataset {391409}
/background_h2/time1     Dataset {391409}
/background_h2/time2     Dataset {391409}
/background_h2/timeslide_id Dataset {391409}
/background_h2/trigger_id1 Dataset {391409}
/background_h2/trigger_id2 Dataset {391409}
/foreground              Group
/foreground/decimation_factor Dataset {14219}
/foreground/fap          Dataset {14219}
/foreground/fap_exc      Dataset {14219}
/foreground/ifar         Dataset {14219}
/foreground/ifar_exc     Dataset {14219}
/foreground/stat         Dataset {14219}
/foreground/template_id  Dataset {14219}
/foreground/time1        Dataset {14219}
/foreground/time2        Dataset {14219}
/foreground/timeslide_id Dataset {14219}
/foreground/trigger_id1  Dataset {14219}
/foreground/trigger_id2  Dataset {14219}
/foreground_h0           Group
/foreground_h0/fap       Dataset {14219}
/foreground_h0/ifar      Dataset {14219}
/foreground_h0/stat      Dataset {14219}
/foreground_h0/template_id Dataset {14219}
/foreground_h0/time1     Dataset {14219}
/foreground_h0/time2     Dataset {14219}
/foreground_h0/trigger_id1 Dataset {14219}
/foreground_h0/trigger_id2 Dataset {14219}
/foreground_h1           Group
/foreground_h1/fap       Dataset {14218}
/foreground_h1/ifar      Dataset {14218}
/foreground_h1/stat      Dataset {14218}
/foreground_h1/template_id Dataset {14218}
/foreground_h1/time1     Dataset {14218}
/foreground_h1/time2     Dataset {14218}
/foreground_h1/trigger_id1 Dataset {14218}
/foreground_h1/trigger_id2 Dataset {14218}
/foreground_h2           Group
/foreground_h2/fap       Dataset {14217}
/foreground_h2/ifar      Dataset {14217}
/foreground_h2/stat      Dataset {14217}
/foreground_h2/template_id Dataset {14217}
/foreground_h2/time1     Dataset {14217}
/foreground_h2/time2     Dataset {14217}
/foreground_h2/trigger_id1 Dataset {14217}
/foreground_h2/trigger_id2 Dataset {14217}
/segments                Group
/segments/H1             Group
/segments/H1/end         Dataset {34}
/segments/H1/start       Dataset {34}
/segments/L1             Group
/segments/L1/end         Dataset {34}
/segments/L1/start       Dataset {34}
/segments/coinc          Group
/segments/coinc/end      Dataset {34}
/segments/coinc/start    Dataset {34}
/segments/foreground_veto Group
/segments/foreground_veto/end Dataset {48133}
/segments/foreground_veto/start Dataset {48133}
```