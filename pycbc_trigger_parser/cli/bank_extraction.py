"""CLI for bank extraction."""
import argparse
import logging
import os

from pycbc_trigger_parser import pycbc_data

from . import cli_utils

logging.getLogger().setLevel(logging.INFO)


def create_bank_extraction_parser():
    """Create cli argparser for bank extraction."""
    parser = argparse.ArgumentParser(description="Extract template bank from pyCBC.")
    parser = cli_utils.add_common_parser_args(parser)
    return parser


def extract_bank(observing_run: int, chunk_number: int, outdir: str):
    """Extract Bank and save them into a file."""
    bank = pycbc_data.get_bank(observing_run=observing_run, chunk_number=chunk_number)
    unfiltered_path = os.path.join(outdir, "template_bank.csv")
    bank.to_csv(path=unfiltered_path)


def main_get_bank():
    """CLI interface to extract bank."""
    cli_args = cli_utils.get_cli_input()
    args = cli_utils.parse_args(cli_args, create_bank_extraction_parser())
    extract_bank(
        chunk_number=args.chunk_number,
        observing_run=args.observing_run,
        outdir=args.outdir,
    )
    logging.info("Completed bank retrieval! •ᴗ•")
