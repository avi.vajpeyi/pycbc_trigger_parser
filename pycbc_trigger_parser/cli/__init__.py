"""Load in CLI functions."""
from .add_catalogue_triggers import (
    add_catalogue_triggers_to_trigger_csv,
    main_add_catalogue_triggers,
)
from .bank_extraction import main_get_bank
from .trigger_extraction import (
    extract_unfiltered_triggers_from_scratch,
    main_get_raw_triggers,
)
from .trigger_filtering import filter_triggers, main_filter_triggers
from .trigger_meta_data_extractor import (
    extract_trigger_meta_data,
    main_extract_trigger_meta_data,
)
from .trigger_plot import main_plot_triggers
