"""CLI for adding catalogue triggers."""
import argparse
import logging

from pycbc_trigger_parser import TriggerTable

from . import cli_utils

logging.getLogger().setLevel(logging.INFO)


def create_add_catalogue_trigger_parser():
    """Create cli argparser for trigger meta data extraction."""
    parser = argparse.ArgumentParser(
        description="Add catalogue triggers to trigger file."
    )
    parser.add_argument("--trigger-csv", type=str, help="Path to triggers csv.")
    parser.add_argument("--observing-run", type=int, help="Observing run # (2 or 3).")
    parser.add_argument("--chunk-number", type=int, help="Chunk # (O2: 1-22, O3:0-?).")

    return parser


def add_catalogue_triggers_to_trigger_csv(trigger_csv, observing_run, chunk_number):
    """Add catalogue triggers to trigger csv."""
    table = TriggerTable.from_csv(trigger_csv)
    table.add_catalogue_events(observing_run=observing_run, chunk=chunk_number)
    table.to_csv(trigger_csv)


def main_add_catalogue_triggers():
    """CLI interface to add catalogue triggers."""
    cli_args = cli_utils.get_cli_input()
    parser = create_add_catalogue_trigger_parser()
    args = cli_utils.parse_args(cli_args, parser)
    add_catalogue_triggers_to_trigger_csv(
        trigger_csv=args.trigger_csv,
        observing_run=args.observing_run,
        chunk_number=args.chunk_number,
    )

    logging.info("Completed adding catalogue triggers to trigger file! •ᴗ•")
