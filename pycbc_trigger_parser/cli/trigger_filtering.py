"""CLI for trigger filtering."""
import argparse
import logging

from pycbc_trigger_parser.injection_parser import InjectionTriggerTable
from pycbc_trigger_parser.trigger_table import TriggerTable


from ..trigger_types import TRIGGER_TYPES, FOREGROUND
from . import cli_utils

UNFILTERED_TRIGGER_CSV = "unfiltered_triggers.csv"
TRIGGER_CSV = "triggers.csv"
MAX_SNR = 50

logging.getLogger().setLevel(logging.INFO)


def create_trigger_filtering_parser():
    """Create cli argparser for trigger filtering."""
    parser = argparse.ArgumentParser(description="Filter trigger csv.")
    parser.add_argument(
        "--unfiltered-csv", type=str, help="Path to the unfiltered triggers csv."
    )
    parser.add_argument(
        "--trigger-type",
        type=str,
        help=f"One of the following: {TRIGGER_TYPES}",
    )
    parser.add_argument(
        "--min-duration", type=float, help="The minimum duration of a trigger."
    )
    parser.add_argument(
        "--max-duration", type=float, help="The maximum duration of a trigger."
    )
    return parser


def filter_triggers(unfiltered_csv, trigger_type, min_duration, max_duration):
    """Filter triggers and save filtered triggers to a file."""
    logging.info(f"Filtering {trigger_type} trigger data from {unfiltered_csv}")
    if trigger_type == "injection":
        trigger_table = InjectionTriggerTable.from_csv(unfiltered_csv)
    else:
        trigger_table = TriggerTable.from_csv(unfiltered_csv)
    logging.info(
        f"Filtering by duration "
        f"(keeping {min_duration}s <= triggers <= {max_duration}s"
    )
    filtered_trigger_table = trigger_table.get_filtered_table(
        H1_template_duration_max=max_duration,
        L1_template_duration_max=max_duration,
        H1_template_duration_min=min_duration,
        L1_template_duration_min=min_duration,
    )
    if trigger_type is not FOREGROUND:
        filtered_trigger_table = filtered_trigger_table.get_filtered_table(
            snr_max=MAX_SNR
        )
    filtered_path = unfiltered_csv.replace(UNFILTERED_TRIGGER_CSV, TRIGGER_CSV)
    filtered_trigger_table.to_csv(filtered_path)


def main_filter_triggers():
    """CLI interface to filter triggers."""
    cli_args = cli_utils.get_cli_input()
    parser = create_trigger_filtering_parser()
    args = cli_utils.parse_args(cli_args, parser)
    filter_triggers(
        unfiltered_csv=args.unfiltered_csv,
        trigger_type=args.trigger_type,
        min_duration=args.min_duration,
        max_duration=args.max_duration,
    )
    logging.info("Completed trigger filtering! •ᴗ•")
