"""CLI for extracting meta data from triggers."""
import argparse
import logging
import os

from pycbc_trigger_parser import (
    InjectionTriggerTable,
    TriggerTable,
    TriggerTableTimes,
    pycbc_data,
)
from pycbc_trigger_parser.injection_parser import INJECTION_JSON

from .. import trigger_types
from . import cli_utils

TIMES_CSV = "times.csv"

logging.getLogger().setLevel(logging.INFO)


def create_trigger_meta_data_extractor_parser():
    """Create cli argparser for trigger meta data extraction."""
    parser = argparse.ArgumentParser(
        description="Create metafiles from trigger file for PE."
    )
    parser.add_argument(
        "--trigger-csv", type=str, help="Path to the unfiltered triggers csv."
    )
    parser.add_argument(
        "--trigger-type",
        type=str,
        help=f"One of the following: {trigger_types.TRIGGER_TYPES}",
    )
    return parser


def extract_trigger_meta_data(trigger_csv: str, trigger_type: str):
    """Save trigger meta data (like timeslide, gps times, and inj parameters."""
    paths = dict(gps_file=None, timeslide_file=None, injection_file=None)
    save_dir = os.path.dirname(trigger_csv)
    if trigger_type == pycbc_data.INJECTION:
        inj_path = _save_injection_file(save_dir, trigger_csv)
        time_paths = _save_time_data(save_dir, trigger_csv)
        paths.update(**inj_path)
        paths.update(**time_paths)
    elif trigger_type == pycbc_data.FOREGROUND:
        time_paths = _save_time_data(save_dir, trigger_csv)
        paths.update(**time_paths)
    elif trigger_type == pycbc_data.BACKGROUND:
        time_paths = _save_time_data(save_dir, trigger_csv, save_timeslides=True)
        paths.update(**time_paths)
    else:
        raise ValueError(
            f"Invalid trigger type. " f"Can be one of {pycbc_data.TRIGGER_TYPES}"
        )
    return paths


def _save_injection_file(save_dir, trigger_csv):
    """Save injection parameter file."""
    injection_path = os.path.join(save_dir, INJECTION_JSON)
    injection_triggers = InjectionTriggerTable.from_csv(trigger_csv)
    injection_triggers.generate_injection_json(
        df=injection_triggers.dataframe, json_path=injection_path
    )
    return dict(injection_file=injection_path)


def _save_time_data(save_dir, trigger_csv, save_timeslides=False):
    """Save time.csv, gps start times, timeslides."""
    trigger_time_csv = os.path.join(save_dir, TIMES_CSV)
    trigger_time = TriggerTableTimes(TriggerTable.from_csv(trigger_csv))
    trigger_time.to_csv(trigger_time_csv)
    gps_file, timeslide_file = trigger_time.save_time_txt_files(
        txt_dir=save_dir, save_timeslides=save_timeslides
    )
    return dict(gps_file=gps_file, timeslide_file=timeslide_file)


def main_extract_trigger_meta_data():
    """CLI interface to filter triggers."""
    cli_args = cli_utils.get_cli_input()
    parser = create_trigger_meta_data_extractor_parser()
    args = cli_utils.parse_args(cli_args, parser)
    extract_trigger_meta_data(
        trigger_csv=args.trigger_csv, trigger_type=args.trigger_type
    )
    logging.info("Completed trigger meta data extraction! •ᴗ•")
