"""Module to plot trigger masses from CLI."""
import argparse
import logging
import os

import bilby
import matplotlib
import plotly.io as pio
import seaborn as sns
from matplotlib import pyplot as plt
from pycbc_trigger_parser import TemplateBank, TriggerTable
from pycbc_trigger_parser.plotting import plot_template_bank

from . import cli_utils

pio.templates.default = "plotly_white"

sns.set(font_scale=1.5)
sns.axes_style("white")
sns.set_style("ticks", {"font.family": "serif"})
matplotlib.use("Agg")

sns.set(font_scale=1.5)
sns.axes_style("white")
sns.set_style("ticks", {"font.family": "serif"})
sns.set_context("talk")

MC_MIN, MC_MAX = None, None
Q_MIN, Q_MAX = None, None
M_MIN, M_MAX = None, None


def create_trigger_plot_parser():
    """Create cli argparser for trigger extraction."""
    parser = argparse.ArgumentParser(description="Plot triggers and prior file.")
    parser.add_argument("--bank-csv", type=str, help="path to csv containing bank.")
    parser.add_argument(
        "--foreground-csv", type=str, help="path to csv containing foreground triggers."
    )
    parser.add_argument(
        "--background-csv", type=str, help="path to csv containing background triggers."
    )
    parser.add_argument(
        "--injection-csv",
        type=str,
        help="path to csv containing software injection triggers",
    )
    parser.add_argument("--prior-file", type=str, help="path to prior file")
    parser.add_argument("--outdir", type=str, help="path to outdir.")

    return parser


def _set_legend_for_bank_plot(ax):
    """Set up legened for the bank plot.

    :param ax:
    :return:
    """
    ax.get_legend().remove()
    bank_patch = ax.scatter([], [], label="Template Bank", marker=".", color="plum")
    prior_patch = ax.plot([], [], label="Prior", linestyle="--", color="k")
    data_plot_kwargs = _get_plot_data_kwargs()
    foreground_patch = ax.scatter(
        [],
        [],
        color=data_plot_kwargs["foreground"]["color"],
        marker=data_plot_kwargs["foreground"]["marker"],
        alpha=data_plot_kwargs["foreground"]["alpha"],
    )
    swinj_patch = ax.scatter(
        [],
        [],
        color=data_plot_kwargs["swinj"]["color"],
        marker=data_plot_kwargs["swinj"]["marker"],
        alpha=data_plot_kwargs["swinj"]["alpha"],
    )
    background_patch = ax.scatter(
        [],
        [],
        color=data_plot_kwargs["background"]["color"],
        marker=data_plot_kwargs["background"]["marker"],
        alpha=data_plot_kwargs["background"]["alpha"],
    )
    handles = (
        bank_patch,
        prior_patch[0],
        foreground_patch,
        swinj_patch,
        background_patch,
    )
    ax.legend(
        handles=handles,
        labels=["Templates", "Prior", "Foreground", "Software Inj", "Background"],
        fontsize="small",
        markerscale=2,
        loc="upper left",
    )
    return handles, ax


def _set_mass_ranges_from_prior(prior_file):
    """Set global mass param from prior file.

    :param prior_file:
    :return:
    """
    prior = bilby.prior.PriorDict(filename=prior_file)
    global MC_MIN, MC_MAX, M_MIN, M_MAX, Q_MIN, Q_MAX
    MC_MIN, MC_MAX = prior["chirp_mass"].minimum, prior["chirp_mass"].maximum
    M_MIN, M_MAX = prior["total_mass"].minimum, prior["total_mass"].maximum
    Q_MIN, Q_MAX = prior["mass_ratio"].minimum, prior["mass_ratio"].maximum


def _get_plot_data_kwargs():
    """Define data plotting kwargs.

    :return:
    """
    return dict(
        background=dict(
            label="Background", color="gray", marker="o", s=15, alpha=0.5  # (5, 2),
        ),
        swinj=dict(
            label="SW injections", color="dodgerblue", marker="h", s=15, alpha=0.5
        ),
        foreground=dict(
            label="Foreground", color="orange", marker="s", s=50, alpha=0.8
        ),
        bank=dict(label="Template Bank", color="plum", marker=".", s=0.5, alpha=0.1),
        prior=dict(colors="k", linestyles="--", linewidths=2),
    )


def plot_triggers(
    bank_csv: str,
    foreground_csv: str,
    background_csv: str,
    injection_csv: str,
    prior_file: str,
    outdir: str,
):
    """Interface to plot triggers.

    :param bank_csv:
    :param foreground_csv:
    :param background_csv:
    :param injection_csv:
    :param prior_file:
    :param outdir:
    :return:
    """
    _set_mass_ranges_from_prior(prior_file)
    data = dict(
        bank=TemplateBank.from_csv(bank_csv),
        background=TriggerTable.from_csv(background_csv),
        swinj=TriggerTable.from_csv(injection_csv),
        foreground=TriggerTable.from_csv(foreground_csv),
    )
    data_kwgs = _get_plot_data_kwargs()
    scatter_points = [
        dict(data=data["bank"].dataframe, plot_kwargs=dict(**data_kwgs["bank"])),
        dict(
            data=data["background"].dataframe,
            plot_kwargs=dict(**data_kwgs["background"]),
        ),
        dict(data=data["swinj"].dataframe, plot_kwargs=dict(**data_kwgs["swinj"])),
        dict(
            data=data["foreground"].dataframe,
            plot_kwargs=dict(**data_kwgs["foreground"]),
        ),
    ]
    countour_lines = [
        dict(
            contour_condition=my_contour_criteria,
            plot_kwargs=dict(**data_kwgs["prior"]),
        )
    ]

    fig, ax_m1m2, ax_mcq, legend_handles = plot_template_bank(
        scatter_points=scatter_points, countour_lines=countour_lines, web_dir=outdir
    )

    _, _ = _set_legend_for_bank_plot(ax_m1m2)

    plot_fname = os.path.join(outdir, "template_bank_masses.png")
    plt.savefig(plot_fname)
    logging.info(f"Trigger plot saved at {plot_fname}")


def my_contour_criteria(m1: float, m2: float, mc: float, q: float, M: float) -> int:
    """Take mass parameters and ses if they fit inside a range."""
    if MC_MIN <= mc <= MC_MAX and Q_MIN <= q <= Q_MAX and M_MIN <= M <= M_MAX:
        return 1
    else:
        return 0


def main_plot_triggers():
    """CLI interface to filter triggers."""
    cli_args = cli_utils.get_cli_input()
    parser = create_trigger_plot_parser()
    args = cli_utils.parse_args(cli_args, parser)
    plot_triggers(
        bank_csv=args.bank_csv,
        foreground_csv=args.foreground_csv,
        background_csv=args.background_csv,
        injection_csv=args.injection_csv,
        prior_file=args.prior_file,
        outdir=args.outdir,
    )
    logging.info("Completed trigger plotting! •ᴗ•")
