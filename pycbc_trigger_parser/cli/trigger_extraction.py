"""CLI for trigger extraction."""
import argparse
import logging
import os
from typing import Optional

from pycbc_trigger_parser import pycbc_data

from .. import trigger_types
from . import cli_utils

UNFILTERED_TRIGGER_CSV = "unfiltered_triggers.csv"

logging.getLogger().setLevel(logging.INFO)


def create_trigger_extraction_parser():
    """Create cli argparser for trigger extraction."""
    parser = argparse.ArgumentParser(
        description="Extract raw triggers from pyCBC searches."
    )
    parser = cli_utils.add_common_parser_args(parser)
    parser.add_argument(
        "--trigger-type",
        type=str,
        help=f"One of the following trigger types: {trigger_types.TRIGGER_TYPES}",
    )
    parser.add_argument(
        "--injection-type",
        type=str,
        help="Some valid injection types: BBH, IMBH, " "BNS...",
    )
    parser.add_argument(
        "--injection-waveform",
        type=str,
        help="Some valid injection waveforms: " "SEOBNRv4, IMRPhenomPv2...",
    )
    return parser


def extract_unfiltered_triggers_from_scratch(
    observing_run: int,
    chunk_number: int,
    outdir: str,
    trigger_type: str,
    injection_type: Optional[str] = None,
    injection_waveform: Optional[str] = None,
):
    """Extract triggers and save them into a file."""
    logging.info(f"Getting paths for {trigger_type}")
    chunk_data = pycbc_data.get_trigger_data(
        observing_run=observing_run,
        chunk_number=chunk_number,
        trigger_type=trigger_type,
        injection_key=injection_type,
        injection_waveform=injection_waveform,
    )
    unfiltered_path = os.path.join(outdir, UNFILTERED_TRIGGER_CSV)
    logging.info(f"Saving extracted triggers in {unfiltered_path}")
    chunk_data.trigger_table.to_csv(path=unfiltered_path)
    return chunk_data


def main_get_raw_triggers():
    """CLI interface to extract triggers."""
    cli_args = cli_utils.get_cli_input()
    args = cli_utils.parse_args(cli_args, create_trigger_extraction_parser())
    logging.info(f"Trigger extraction args: {args}")
    extract_unfiltered_triggers_from_scratch(
        chunk_number=args.chunk_number,
        observing_run=args.observing_run,
        outdir=args.outdir,
        trigger_type=args.trigger_type,
        injection_type=args.injection_type,
        injection_waveform=args.injection_waveform,
    )
    logging.info("Completed trigger retrieval! •ᴗ•")
