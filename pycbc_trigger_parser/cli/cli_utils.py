"""Module to assist with command line input."""
import os
import sys

from ..trigger_types import TRIGGER_TYPES


def parse_args(input_args, parser):
    """Parse the cli argparser.

    :param input_args:
    :param parser:
    :return:
    """
    args, _ = parser.parse_known_args(input_args)
    args_dict = vars(args)
    if "trigger_type" in args_dict.keys() and args.trigger_type not in TRIGGER_TYPES:
        raise ValueError(
            f"Table type {args.trigger_type} is invalid."
            f"Valid types {TRIGGER_TYPES}."
        )
    if "outdir" in args_dict.keys():
        os.makedirs(args.outdir, exist_ok=True)
    return args


def add_common_parser_args(parser):
    """Add common args."""
    parser.add_argument(
        "--outdir", type=str, help="The outdir where the trigger csv will be stored."
    )
    parser.add_argument("--observing-run", type=int, help="Observing run # (2 or 3).")
    parser.add_argument("--chunk-number", type=int, help="Chunk # (O2: 1-22, O3:0-?).")
    return parser


def get_cli_input():
    """Wrap sys.argv to make input from cli consistent."""
    return sys.argv[1:]
