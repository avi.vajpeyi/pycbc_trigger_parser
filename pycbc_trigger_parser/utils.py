"""Module with helper functions."""

import ast
import logging
import re

import h5py
import numpy as np
import pandas as pd

GPS_TIME = "gps_time"
AVG_TIMESLID_TIME = "avg_timeslid_time"
TRIGGER_TIME = "trigger_time"
logging.getLogger().setLevel(logging.INFO)


def filter_dataframe(dataframe: pd.DataFrame, **kwargs):
    """Filter dataframe based on min-max kwargs.

    :param dataframe: the dataframe being filtered
    :param kwargs: the key of the dataframe followed by '_min' or '_max' and the value
    that the key should be set to. For example,
    """
    before_len = len(dataframe)
    if kwargs:
        strcond = "&".join((make_cond(k, v) for k, v in kwargs.items()))
        dataframe = dataframe.query(strcond)
    after_len = len(dataframe)
    logging.info(
        f"Filtered with the condition: {strcond}. {before_len} Triggers --> {after_len} Triggers"
    )
    return dataframe


def make_cond(k, v):
    """Create a string format of a key and value condition.

    :param k:
    :param v:
    :return:
    """
    if len(k) < 5:
        raise (ValueError("Arg too short {}".format(k)))
    if k.endswith("_min"):
        return "({}>={})".format(k[:-4], v)
    if k.endswith("_max"):
        return "({}<={})".format(k[:-4], v)


def get_combined_snr(snr_list):
    """Sqrt(sum over i ( SNR[i]^2)).

    snr_list = list of [i] snrs that are to be combined
    """
    snr_list = np.abs(snr_list) ** 2
    combined_snr = np.sqrt(np.sum(snr_list, axis=0))
    return combined_snr


def correct_m1_m2(m1, m2):
    """Return m1 m2 such that m1 > m2.

    :param m1: component mass 1
    :param m2: component mass 2
    :return:
    """
    if m1 >= m2:
        return m1, m2
    else:
        return m2, m1


def remove_unnamed_columns(df: pd.DataFrame) -> pd.DataFrame:
    """Remove any unnamed columns from dataframe.

    :param df: dataframe whose unnamed columns to be removed
    :return: pd.DataFrame
    """
    return df.loc[:, ~df.columns.str.contains("^Unnamed")]


def adjust_trigger_dataframes(trigger_df, time_df, other_df):
    assert not other_df.empty
    if time_df.empty or trigger_df.empty:
        logging.warning("No trigger PyCBC data present, only using evidences.")

    time_df = remove_unnamed_columns(time_df)
    trigger_df.rename(columns={"Unnamed: 0": "TriggerIdx"}, inplace=True)
    other_df.rename(columns={"Unnamed: 0": "Idx"}, inplace=True)
    other_df[TRIGGER_TIME] = other_df[GPS_TIME]
    other_df[GPS_TIME] = other_df[GPS_TIME] - 2

    logging.info(f"Pairing {len(trigger_df)} triggers with {len(other_df)} results.")

    # standardising the data
    other_df[GPS_TIME] = round(other_df[GPS_TIME], 4)

    # sorting dataframes
    other_df = other_df.sort_values(by=GPS_TIME)
    return trigger_df, time_df, other_df


def get_uncommon_rows(df1, df2, col):
    """
    :param df1: dataframe with extra rows
    :param df2: dataframe
    :param col: the key of the column to be compared
    :return: dataframe with the extra rows in df1
    """
    cols = df1.columns.tolist()
    common = pd.merge(df1, df2, on=cols, how="inner", suffixes=(False, False))
    uncommon_rows = df1[(~df1[col].isin(common[col]))]
    return uncommon_rows


def merge_triggers_with_other(trigger_df, time_df, other_df):
    # merge times and trigger info
    trig_and_time_df = trigger_df.merge(
        time_df, how="inner", left_index=True, right_index=True
    )
    trig_and_time_df[GPS_TIME] = round(trig_and_time_df[AVG_TIMESLID_TIME], 4)
    trig_and_time_df = trig_and_time_df.sort_values(by=GPS_TIME)

    # merging with other df
    if trig_and_time_df.empty is False:
        trig_and_time_df = pd.merge_asof(
            trig_and_time_df, other_df, on=GPS_TIME, direction="nearest", tolerance=0.01
        )
    else:
        cols = list(set(list(trig_and_time_df.columns) + list(other_df.columns)))
        trig_and_time_df = pd.DataFrame(columns=cols)

    if "label" in trig_and_time_df:
        trig_and_time_df["label"] = str(trig_and_time_df["label"])

    # dropping rows which don't have matching GPS times in other_df and trigger_df
    paired_rows = trig_and_time_df.dropna()  # df without the NAs
    unpaired_rows = trig_and_time_df[
        ~trig_and_time_df.index.isin(paired_rows.index)
    ]  # df with the NAs
    with pd.option_context("float_format", "{:f}".format):
        if len(unpaired_rows) > 0:
            percent = 100 * len(unpaired_rows) / len(trigger_df)
            count_str = f"{len(unpaired_rows)}/{len(trigger_df)}"
            logging.warning(
                f"Cant pair {percent:.1f}% triggers ({count_str}) with PyCBC ."
            )
            logging.debug(
                f"Times for dropped triggers:\n"
                f"{unpaired_rows.avg_timeslid_time.tolist()}"
            )
        if len(paired_rows) == 0:
            logging.critical(f"NO TRIGGERS PAIRED WITH EVIDENCES")
    logging.info(f"Paired {len(paired_rows)}/{len(trigger_df)} trigger results.")

    return paired_rows, unpaired_rows


def add_extra_evidences_to_trigger_dataframe(other_df, paried_rows):
    orig_len = len(paried_rows)
    extra_evidences = get_uncommon_rows(df1=other_df, df2=paried_rows, col=GPS_TIME)
    paried_rows = paried_rows.append(extra_evidences, ignore_index=True, sort=False)
    cols = other_df.columns.tolist()
    cols.remove(GPS_TIME)
    paried_rows = paried_rows.drop_duplicates(subset=cols)
    paried_rows = paried_rows.sort_values(by=GPS_TIME)
    if len(paried_rows) > orig_len:
        logging.info(
            f"Adding {len(extra_evidences)} evidences to trigger results "
            f"with GPS times of : "
            f"{extra_evidences[GPS_TIME].tolist()}"
        )
    return paried_rows


def combine_trigger_and_extra_data_using_gpstime(trigger_df, time_df, other_df):
    """Combine dfs based on GPS time.

    :param trigger_df:
    :param time_df:
    :param other_df:
    :return: paired_rows, unpaired_rows
    """
    try:
        trigger_df, time_df, other_df = adjust_trigger_dataframes(
            trigger_df, time_df, other_df
        )
        paired_rows, unpaired_rows = merge_triggers_with_other(
            trigger_df, time_df, other_df
        )
        paired_rows = add_extra_evidences_to_trigger_dataframe(other_df, paired_rows)
    except Exception as e:
        logging.error(e)
        logging.warning("Returning empty rows")
        paired_rows, unpaired_rows = pd.DataFrame(), pd.DataFrame()
    return paired_rows, unpaired_rows


def read_hdf_file(filepath):
    """Read HDF files and returns a filepointer to the file.

    Provides a useful log if users get a 'PermissionDenied' (OsErro no 17) error.

    :param filepath: path to the hdf file
    :return: filepointer to the hdf.
    """
    try:
        filepointer = h5py.File(name=filepath, mode="r")
        return filepointer
    except PermissionError as e:
        logging.error(
            "Permission denied! Try running `ligo-proxy-init your.user`.\n"
            f"Error message: {e}"
        )


def convert_string_to_dict(string):
    """Convert a string repr of a string to a python dictionary."""
    string = string.replace('"', "").replace("'", "")
    # Convert equals to colons
    string = string.replace("=", ":")
    string = string.replace(" ", "")
    # Force double quotes around everything
    string = re.sub(r'([A-Za-z/\.0-9][^\[\],:"}]*)', r'"\g<1>"', string)
    string = string.replace('""', '"')
    # Evaluate as a dictionary of str: str
    dic = ast.literal_eval(string)
    return dic
