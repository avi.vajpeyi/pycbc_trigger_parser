"""PyCBC triggerlist module."""
import logging

import numpy as np

from .utils import read_hdf_file

logging.basicConfig(level=logging.INFO)

SNR = "snr"
TRIGGER_TIME = "trigger_time"
END_TIME = "end_time"
START_TIME = "start_time"
TEMPLATE_DURATION = "template_duration"
TRIGGER_ID = "trigger_id"

# The following are adapted from Bilby Pipe's values
SEGMENT_DURATION = 4
POST_TRIGGER_DURATION = 2


class TriggerList(object):
    """Store list of triggers."""

    def __init__(
        self,
        ifo_label,
        snr,
        template_duration,
        start_time,
        id_num,
        post_trigger_duration,
        segment_duration,
    ):
        """Initialise trigger list.

        :param ifo_label:
        :param snr:
        :param template_duration:
        :param start_time:
        :param id_num:
        :param post_trigger_duration:
        :param segment_duration:
        """
        self.ifo_label = ifo_label
        self.snr = snr
        self.template_duration = template_duration
        self.start_time = start_time
        self.end_time = start_time + segment_duration
        self.trigger_time = self.end_time - post_trigger_duration
        self.id_num = id_num
        self.post_trigger_duration = post_trigger_duration
        self.segment_duration = segment_duration

    @classmethod
    def from_pycbc_hdf_trigger_merge_file(cls, trigger_merge_file, id_num):
        """Generate list from hdfs.

        :param trigger_merge_file:
        :param id_num:
        :return:
        """
        logging.info("Reading {}".format(trigger_merge_file))
        trigger_file = read_hdf_file(trigger_merge_file)

        ifo_label = list(trigger_file.keys()).pop()  # either H1 or L1
        trigger_data = trigger_file.get(ifo_label)
        snr = np.take(trigger_data.get(SNR), indices=id_num)  # pyCBC's SNR

        # NOTE: pyCBC's 'end time' is the trigger time!!
        trigger_time = np.take(trigger_data.get(END_TIME), indices=id_num)
        template_duration = np.take(trigger_data.get(TEMPLATE_DURATION), indices=id_num)

        # equation taken from bilby_pipe
        # https://git.ligo.org/lscsoft/bilby_pipe/blob/master/bilby_pipe/input.py#L233
        start_time = (trigger_time + POST_TRIGGER_DURATION) - SEGMENT_DURATION
        return cls(
            ifo_label=ifo_label,
            snr=snr,
            template_duration=template_duration,
            start_time=start_time,
            id_num=id_num,
            post_trigger_duration=POST_TRIGGER_DURATION,
            segment_duration=SEGMENT_DURATION,
        )

    @classmethod
    def from_dataframe(cls, trigger_dataframe, ifo_label):
        """Generate list from dataframe.

        :param trigger_dataframe:
        :param ifo_label:
        :return:
        """
        label = "_".join([ifo_label, "{}"])
        return cls(
            ifo_label=ifo_label,
            snr=trigger_dataframe[label.format(SNR)],
            template_duration=trigger_dataframe[label.format(TEMPLATE_DURATION)],
            start_time=trigger_dataframe[label.format(START_TIME)],
            id_num=trigger_dataframe[label.format(TRIGGER_ID)],
            post_trigger_duration=POST_TRIGGER_DURATION,
            segment_duration=SEGMENT_DURATION,
        )

    def to_dict(self):
        """Convert list to dict.

        :return:
        """
        label = "_".join([self.ifo_label, "{}"])
        return {
            label.format(SNR): self.snr,
            label.format(START_TIME): self.start_time,
            label.format(TRIGGER_TIME): self.trigger_time,
            label.format(TEMPLATE_DURATION): self.template_duration,
            label.format(TRIGGER_ID): self.id_num,
        }
