"""Module for storing trigger table times."""

import logging
import os

import numpy as np
import pandas as pd

from .trigger_table import TriggerTable

H1 = "H1"
L1 = "L1"
TIMESLIDE_VAL = "timeslide_val"

H1_TIME = "H1_start_time"
L1_TIME = "L1_start_time"
H1_TIME_END = "H1_end_time"
L1_TIME_END = "L1_end_time"

H1_TIMESLID_TIME = "H1_timeslid_time"
L1_TIMESLID_TIME = "L1_timeslid_time"
H1_TIMESLID_TIME_END = "H1_timeslid_time_end"
L1_TIMESLID_TIME_END = "L1_timeslid_time_end"

AVG_TIMESLID_TIME = "avg_timeslid_time"
H1_TIMSLIDE_VAL = "H1_timslide_val"
L1_TIMSLIDE_VAL = "L1_timslide_val"

L1_DELTA_TIME = "L1_delta_time"
H1_DELTA_TIME = "H1_delta_time"

GPS_FILE = "gps_start_time.txt"
TIMESLIDE_FILE = "timeslide.txt"


class TriggerTableTimes(object):
    """Store trigger times."""

    def __init__(self, trigger_table: TriggerTable):
        """Collect trigger time data.

        :param trigger_table:
        """
        self.ifo_trigger = trigger_table.ifo_trigger
        self.timeslide_value = trigger_table.timeslide_value
        self.timeslid_start_times = self.__get_timeslid_start_times()
        self.untimeslid_start_times = self.__get_untimeslid_start_times()
        self.average_timeslid_start_time = self.__get_avg_timeslid_start_time()
        self.new_timeslides = self.__get_new_timeslides_based_on_avg_time()

    def __get_timeslid_start_times(self) -> dict:
        """Get trigger start time (with the timeslide applied).

        PyCBC stores the trigger.start_time as the timeslid value.

        NOTE: same value as `__get_untimeslid_start_times` if timeslide == 0

        :return: {H1: timeslid_time_h1, L1: timeslid_time_l1}
        """
        return {
            ifo_name: trigger.start_time  # + self.timeslide_value
            for ifo_name, trigger in self.ifo_trigger.items()
        }

    def __get_untimeslid_start_times(self) -> dict:
        """Get the times of the trigger (without the timeslide).

        If timeslide value == 0
            untimeslid_start_time  = trigger.start_time
        otherwise:
            untimeslid_start_time = trigger.start_time  - timeslide_value

        NOTE: same value as `__get_timeslid_start_times` if timeslide == 0

        :return: {H1: time_h1, L1: time_l1}
        """
        return {
            ifo_name: trigger.start_time - self.timeslide_value
            for ifo_name, trigger in self.ifo_trigger.items()
        }

    def __get_avg_timeslid_start_time(self) -> np.ndarray:
        """Return the avg timeslid time of H1 L1 detectors.

        :return: list of (timeslid_time_h1 + timeslid_time_l1)/2
        """
        avg_time = "avg_time"

        # convert h1_t0, l1_t0 times into a dataframe
        t0_dict = self.__get_timeslid_start_times()
        t0_dataframe = pd.DataFrame.from_dict(t0_dict)

        # add a new column to time dataframe for the avg time
        t0_dataframe[avg_time] = np.zeros(len(t0_dataframe))

        # average time = h1_t0, l1_t0) / 2
        for key in t0_dict.keys():
            t0_dataframe[avg_time] += t0_dataframe[key]
        t0_dataframe[avg_time] = t0_dataframe[avg_time] / len(t0_dict.keys())

        # return the list of average times
        return t0_dataframe[avg_time].values

    def __get_new_timeslides_based_on_avg_time(self) -> np.ndarray:
        """Create timeslide values based on avg timeslid time and individual times.

        :return: [H1_timeslides, L1_timeslides]
        """
        new_timeslides = {
            ifo: t0 - self.average_timeslid_start_time
            for ifo, t0 in self.timeslid_start_times.items()
        }
        new_timeslide_array = np.array([new_timeslides[H1], new_timeslides[L1]])
        return np.transpose(new_timeslide_array)

    def to_dataframe(self):
        """Convert to df.

        Dataframe columns:

        H1_time:
        L1_time:
        H1_time_end:
        L1_time_end:
        timeslide_val:
        H1_timeslid_time:
        L1_timeslid_time:
        avg_timeslid_time:
        H1_timslide_val:
        L1_timslide_val:
        L1_delta_time:
        H1_delta_time:


        :return: Dataframe of the time info.
        """
        logging.info("Generating trigger time dataframe")
        # from trigger list
        h1_timeslid_time = self.timeslid_start_times[H1]
        l1_timeslid_time = self.timeslid_start_times[L1]

        # h1_untimeslid_time = self.untimeslid_start_times[H1]
        # l1_untimeslid_time = self.untimeslid_start_times[L1]

        # TriggerTableTimes calculations
        h1_timeslide = self.new_timeslides[:, 0]
        l1_timeslide = self.new_timeslides[:, 1]
        new_h1_timeslid_time = self.average_timeslid_start_time + h1_timeslide
        new_l1_timeslid_time = self.average_timeslid_start_time + l1_timeslide

        # make sure new timeslid vales math up with original timeslid values
        h1_delta_time = h1_timeslid_time - new_h1_timeslid_time
        l1_delta_time = l1_timeslid_time - new_l1_timeslid_time
        assert sum(h1_delta_time) < 0.0001
        assert sum(l1_delta_time) < 0.0001

        dataframe = pd.DataFrame.from_dict(
            {
                # H1_TIME: h1_untimeslid_time,
                # L1_TIME: l1_untimeslid_time,
                # H1_TIME_END: h1_untimeslid_time - self.timeslide_value,
                # L1_TIME_END: l1_untimeslid_time - self.timeslide_value,
                TIMESLIDE_VAL: self.timeslide_value,
                H1_TIMESLID_TIME: h1_timeslid_time,
                L1_TIMESLID_TIME: l1_timeslid_time,
                H1_TIMESLID_TIME_END: self.ifo_trigger[H1].end_time,
                L1_TIMESLID_TIME_END: self.ifo_trigger[L1].end_time,
                AVG_TIMESLID_TIME: self.average_timeslid_start_time,
                H1_TIMSLIDE_VAL: h1_timeslide,
                L1_TIMSLIDE_VAL: l1_timeslide,
            }
        )
        return dataframe

    def to_csv(self, path):
        """Save as CSV.

        :param path:
        :return:
        """
        self.to_dataframe().to_csv(path)

    def save_time_txt_files(self, txt_dir, save_timeslides=False):
        """Save times in txt.

        :param txt_dir:
        :return:
        """
        logging.info("Saving trigger time data in txt files")
        if not os.path.isdir(txt_dir):
            os.makedirs(txt_dir, exist_ok=True)

        # save avg GPS start time
        gps_start_time_filename = os.path.join(txt_dir, GPS_FILE)
        np.savetxt(gps_start_time_filename, self.average_timeslid_start_time, fmt="%f")
        logging.info(f"Saved GPS start times in {gps_start_time_filename}")

        timeslide_filename = None
        if save_timeslides:
            # save new timeslides value
            timeslide_filename = os.path.join(txt_dir, TIMESLIDE_FILE)
            np.savetxt(timeslide_filename, self.new_timeslides, fmt="%f")
            logging.info(f"Saved timeslides in {timeslide_filename}")

        return gps_start_time_filename, timeslide_filename
