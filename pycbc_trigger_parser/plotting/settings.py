"""Settings for plotting."""

from distutils.spawn import find_executable

import matplotlib.pyplot as plt


def setup_text_plots(fontsize=8, usetex=True):
    """Configure matplotlib settings.

    This function adjusts matplotlib settings so that all figures in the
    have a uniform format and look.
    """
    import matplotlib
    from distutils.version import LooseVersion

    matplotlib.rc("legend", fontsize=fontsize, handlelength=3)
    matplotlib.rc("axes", titlesize=fontsize)
    matplotlib.rc("axes", labelsize=fontsize)
    matplotlib.rc("xtick", labelsize=fontsize)
    matplotlib.rc("ytick", labelsize=fontsize)
    matplotlib.rc("text", usetex=usetex)
    matplotlib.rc(
        "font",
        size=fontsize,
        family="serif",
        style="normal",
        variant="normal",
        stretch="normal",
        weight="normal",
    )
    matplotlib.rc("patch", force_edgecolor=True)
    if LooseVersion(matplotlib.__version__) < LooseVersion("3.1"):
        matplotlib.rc("_internal", classic_mode=True)
    else:
        # New in mpl 3.1
        matplotlib.rc("scatter", edgecolors="b")
    matplotlib.rc("grid", linestyle=":")
    matplotlib.rc("errorbar", capsize=3)
    matplotlib.rc("image", cmap="viridis")
    matplotlib.rc("axes", xmargin=0)
    matplotlib.rc("axes", ymargin=0)
    matplotlib.rc("xtick", direction="in")
    matplotlib.rc("ytick", direction="in")
    matplotlib.rc("xtick", top=True)
    matplotlib.rc("ytick", right=True)
    matplotlib.rc("figure", figsize=(10, 5))


def get_plot_marker_kwargs():
    """Return a dict with marker kwargs."""
    return dict(
        background={
            "label": "Background",
            "color": "gray",
            "marker": ".",  # (5, 2),
            "lw": 3,
            "alpha": 0.2,
        },
        swinj={
            "label": "SW injections",
            "color": "skyblue",
            "lw": 2,
            "marker": "h",
            "alpha": 0.4,
        },
        injection={
            "label": "SW injections",
            "color": "skyblue",
            "lw": 2,
            "marker": "h",
            "alpha": 0.4,
        },
        foreground={
            "label": "Foreground",
            "color": "orange",
            "lw": 2,
            "marker": "s",
            "markersize": 8,
            "alpha": 0.8,
        },
        S190521g={
            "label": "S190521g",
            "color": "crimson",
            "marker": "*",
            "markersize": 15,
            "lw": 3,
        },
        line={"color": "crimson", "alpha": 0.5, "lw": 2},
        legend={
            "loc": "upper right",
            "numpoints": 1,
            "fontsize": 15,
            "framealpha": 0.2,
        },
    )


def check_for_tex():
    """Check if latex installed and turn on if available for use.

    :return:
    """
    if find_executable("latex"):
        plt.rc("text.latex", preamble=r"\usepackage{cmbright}")
        plt.rc("text", usetex=True)
    else:
        plt.rc("text", usetex=False)
