"""Bank_plot_utils: Module with helper functions for the bank plot."""

import logging
from typing import Tuple

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from bilby.gw import conversion
from tqdm import tqdm

matplotlib.use("agg")


def get_criteria_grid(
    xmin, xmax, ymin, ymax
) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    """Provide x, y, z coords based on min and max vals.

    :param xmin: min x val
    :param xmax: max x val
    :param ymin: min y val
    :param ymax: max y val
    :return: Tuple(x,y,z)
    """
    xs = np.linspace(xmin, xmax, 1000)
    ys = np.linspace(ymin, ymax, 1000)[::-1]
    x, y = np.meshgrid(xs, ys)
    z = np.zeros(shape=(len(xs), len(ys)))
    return x, y, z


def get_m1m2_grid(m1_range, m2_range, filtering_criteria):
    """Return a meshgrid of m1, m2, z points based on filtering criteria.

    The returned meshgrid can be used to create a contour plot based on the filtering
    criteria

    :param m1_range:
    :param m2_range:
    :param filtering_criteria:
    :return:
    """
    logging.info("plotting m1-m2 contour")
    xs = np.linspace(m1_range[0], m1_range[1], 1000)
    ys = np.linspace(m2_range[0], m2_range[1], 1000)[::-1]
    m1, m2 = np.meshgrid(xs, ys)
    z = np.zeros(shape=(len(xs), len(ys)))
    for nrx, loop_m1 in enumerate(tqdm(xs)):
        for nry, loop_m2 in enumerate(ys):
            if loop_m2 > loop_m1:
                pass  # by definition, we choose only m2 smaller than m1
            if loop_m2 < loop_m1:
                mc = conversion.component_masses_to_chirp_mass(loop_m1, loop_m2)
                M = conversion.component_masses_to_total_mass(loop_m1, loop_m2)
                q = conversion.component_masses_to_mass_ratio(loop_m1, loop_m2)
                if filtering_criteria(loop_m1, loop_m2, mc, q, M) == 1:
                    z[nry][nrx] = 1
    return m1, m2, z


def get_qmc_grid(q_range, mc_range, filtering_criteria):
    """Return a meshgrid of q, mc, z points based on filtering criteria.

    The returned meshgrid can be used to create a contour plot based on the filtering
    criteria


    :param q_range:
    :param mc_range:
    :param filtering_criteria:
    :return:
    """
    logging.info("plotting q-mc contour")
    xs = np.linspace(q_range[0], q_range[1], 1000)
    ys = np.linspace(mc_range[0], mc_range[1], 1000)[::-1]
    q, mc = np.meshgrid(xs, ys)
    z = np.zeros(shape=(len(xs), len(ys)))
    for nrx, loop_q in enumerate(tqdm(xs)):
        for nry, loop_mc in enumerate(ys):
            M = conversion.chirp_mass_and_mass_ratio_to_total_mass(loop_mc, loop_q)
            m1, m2 = conversion.total_mass_and_mass_ratio_to_component_masses(loop_q, M)
            if filtering_criteria(m1, m2, loop_mc, loop_q, M) == 1:
                z[nry][nrx] = 1
    return q, mc, z


def configure_matplotlib_settings_for_bank_plot():
    """Adjust matplolib rc settings for the bank_plot.

    :return:
    """
    plt.grid(True)
    font = {
        "family": "sans-serif",
        "sans-serif": ["Helvetica"],
        "weight": "bold",
        "size": 22,
    }
    matplotlib.rc("font", **font)
    plt.rc("text", usetex=False)

    plt.rc("xtick", labelsize=20)
    plt.rc("ytick", labelsize=20)
    plt.rc("axes", labelsize=20)
    plt.rc("axes", linewidth=2)
    # set tick length
    plt.rc("xtick.major", size=10)
    plt.rc("xtick.minor", size=5)
    plt.rc("ytick.major", size=10)
    plt.rc("ytick.minor", size=5)
    # leave minor ticks on
    plt.rc("xtick.minor", visible=True)
    plt.rc("ytick.minor", visible=True)

    # mirroring axes
    plt.rc("xtick", top=True)
    plt.rc("ytick", right=True)
    plt.rc("xtick", direction="in")
    plt.rc("ytick", direction="in")
    plt.figure(figsize=(10, 12))


def setup_bank_plot_figure(m1_range, m2_range, mc_range, q_range):
    """Set up axes for bank-plot.

    :param m1_range:
    :param m2_range:
    :param mc_range:
    :param q_range:
    :return:
    """
    axis_label_kwargs = dict(fontsize="x-large")
    nrows, ncols = 2, 1

    # create axes and figure
    fig, (ax_m1m2, ax_mcq) = plt.subplots(nrows=nrows, ncols=ncols, figsize=(8, 10))
    # ax_m1m2 = plt.subplot(nrows, ncols, 1)
    # ax_mcq = plt.subplot(nrows, ncols, 2)

    # set labels
    ax_m1m2.set_xlabel("Mass 1", **axis_label_kwargs)
    ax_m1m2.set_ylabel("Mass 2", **axis_label_kwargs)
    ax_mcq.set_xlabel("q", **axis_label_kwargs)
    ax_mcq.set_ylabel("Chirp Mass", **axis_label_kwargs)

    # set scales
    ax_m1m2.set_yscale("log")
    ax_m1m2.set_xscale("log")
    ax_mcq.set_yscale("log")
    ax_mcq.set_xscale("linear")

    # set scale limits
    ax_m1m2.set_xlim(m1_range[0], m1_range[1])
    ax_m1m2.set_ylim(m2_range[0], m2_range[1])
    ax_mcq.set_xlim(q_range[0], q_range[1])
    ax_mcq.set_ylim(mc_range[0], mc_range[1])

    return fig, ax_m1m2, ax_mcq


def set_legend_for_bank_plot(ax):
    """Set up legened for the bank plot.

    :param ax:
    :return:
    """
    bank_patch = ax.scatter([], [], label="Template Bank", marker=".", color="r")
    prior_patch, = ax.plot([], [], label="Prior", linestyle="--", color="b")
    handles = [bank_patch, prior_patch]
    ax.legend(handles=handles, fontsize="large", markerscale=3)
    return handles
