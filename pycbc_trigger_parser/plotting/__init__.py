"""Sets imports for plotting."""

from .bank_plot import plot_interactive_template_bank, plot_template_bank
from .settings import setup_text_plots
