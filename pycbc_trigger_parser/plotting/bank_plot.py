"""Module to create plots of template bank."""

import logging
import os

import plotly.io as pio

from . import bank_plot_utils
from .bank_plot_utils import get_m1m2_grid, get_qmc_grid
from .settings import setup_text_plots

# from plotly import graph_objs as go
# from plotly.offline import plot


setup_text_plots(fontsize=14, usetex=False)


def plot_template_bank(scatter_points, countour_lines=[], web_dir="."):
    """Create figure of the template bank.

    :param m1: list of mass 1 values
    :param m2: list of mass 2 values
    :param mc: list of mass 2 values
    :param q: list of mass 2 values
    :param M: list of mass 2 values
    :param filtering_criteria: a function that takes in m1, m2, q, M, Mc and returns
    1 or 0 if the parameters passed lie inside some criteria defined by the function
    :param web_dir: root dir where template bank plot is saved
    :return:
    """
    logging.info("Plotting template bank")
    bank_plot_utils.configure_matplotlib_settings_for_bank_plot()

    m1_range = [1, 500]
    m2_range = [1, 200]
    mc_range = [1, 200]
    q_range = [0.005, 1.0005]

    fig, ax_m1m2, ax_mcq = bank_plot_utils.setup_bank_plot_figure(
        m1_range, m2_range, mc_range, q_range
    )

    legend_handles = bank_plot_utils.set_legend_for_bank_plot(ax_m1m2)

    if countour_lines:
        # add contour lines
        for countour_line_data in countour_lines:
            conotur_filter = countour_line_data["contour_condition"]
            plotting_kwargs = countour_line_data["plot_kwargs"]
            m1_line, m2_line, m1m2_z = get_m1m2_grid(m1_range, m2_range, conotur_filter)
            q_line, mc_line, mcq_z = get_qmc_grid(q_range, mc_range, conotur_filter)
            ax_m1m2.contour(m1_line, m2_line, m1m2_z, [0], **plotting_kwargs)
            ax_mcq.contour(q_line, mc_line, mcq_z, [0], **plotting_kwargs)

    # adding scatter points
    for scatter_data in scatter_points:
        data = scatter_data["data"]
        plotting_kwargs = scatter_data["plot_kwargs"]
        ax_m1m2.scatter(data["mass_1"], data["mass_2"], **plotting_kwargs)
        ax_mcq.scatter(data["mass_ratio"], data["mass_chirp"], **plotting_kwargs)

    fig.tight_layout()
    fig.savefig(os.path.join(web_dir, "template_bank_masses.png"))
    return fig, ax_m1m2, ax_mcq, legend_handles


def plot_interactive_template_bank(scatter_list, filename="plotly_bank.html"):
    """Scatterplot of the masses.

    :param df:
    :param filename:
    :param title:
    :return:
    """
    pio.templates.default = "plotly_white"
    #
    # logging.info("Plotting {}".format(filename))
    # stat_traces = []
    # bcr_traces = []
    #
    # x_data_key = {"BCR": bcr_traces, "stat": stat_traces}
    # y_data_key = "far"
    # z_data_key = "gps_time"
    #
    # x_keys = ["mass_1", "q"]
    # y_keys = ["mass_2", "mass_chirp"]
    #
    # for x_key, y_key in zip(x_keys, y_keys):
    #
    #     for scatter in scatter_list:
    #         kwargs = scatter["kwargs"]
    #         df = scatter["data"]
    #
    #         point_labels = []
    #         for i in range(len(df)):
    #             point_i_label = (
    #                 f"({x_key}={df[x_key].values[i]:.2f},"
    #                 f"({y_key}={df[y_key].values[i]:.2f},"
    #                 f"({z_data_key}={df[z_data_key].values[i]:.2f}"
    #             )
    #             point_labels.append(point_i_label)
    #
    #         visible = False
    #         if x_key == x_keys[0]:
    #             visible = True
    #
    #         trace = go.Scattergl(
    #             x=df[x_key],
    #             y=df[y_key],
    #             text=point_labels,
    #             mode="markers",
    #             hoverinfo="text",
    #             name=kwargs["name"],
    #             marker=dict(color=kwargs["color"], size=kwargs["size"]),
    #             visible=visible,
    #         )
    #
    #         trace_list.append(trace)
    #         x_data_key.update({ranking_stat_key: trace_list})
    #
    # # Edit the layout
    #
    # axis_kwargs = dict(
    #     mirror=True,
    #     ticks="inside",
    #     tickwidth=2,
    #     showline=True,
    #     linewidth=2,
    #     linecolor="black",
    #     gridcolor="gainsboro",
    # )
    #
    # layout = go.Layout(
    #     yaxis=go.layout.YAxis(
    #         showexponent="all",
    #         exponentformat="e",
    #         title="FAR",
    #         type="log",
    #         **axis_kwargs,
    #     ),
    #     xaxis=go.layout.XAxis(
    #         title="Ranking Stat",
    #         **axis_kwargs,
    #         zeroline=True,
    #         zerolinewidth=2,
    #         zerolinecolor="midnightblue",
    #     ),
    #     showlegend=True,
    #     font=dict(size=30),
    # )
    #
    # fig = go.Figure(data=x_data_key["BCR"] + x_data_key["stat"], layout=layout)
    #
    # fig.update_layout(
    #     updatemenus=[
    #         go.layout.Updatemenu(
    #             type="buttons",
    #             active=0,
    #             direction="right",
    #             x=1,
    #             y=1.2,
    #             buttons=list(
    #                 [
    #                     dict(
    #                         label="BCR",
    #                         method="update",
    #                         args=[
    #                             {"visible": [True, True, True, False, False, False]},
    #                             {"title": "FAR vs BCR", "xaxis": {"title": "BCR"}},
    #                         ],
    #                     ),
    #                     dict(
    #                         label="pyCBC Stat",
    #                         method="update",
    #                         args=[
    #                             {"visible": [False, False, False, True, True, True]},
    #                             {
    #                                 "title": "FAR vs pyCBC Stat",
    #                                 "xaxis": {"title": "pyCBC Stat"},
    #                             },
    #                         ],
    #                     ),
    #                 ]
    #             ),
    #         )
    #     ]
    # )
    #
    # # Set title
    # fig.update_layout(title_text="FAR vs BCR")
    #
    # graph_url = plot(fig, filename=filename, auto_open=False, include_mathjax="cdn")
    # return os.path.abspath(graph_url)
