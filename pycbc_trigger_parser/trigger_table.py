"""PyCBC Trigger Table module."""
import logging
import os
from typing import Optional

import numpy as np
import pandas as pd

from .catalogue_events import CatalogueEvents
from .template_bank import TemplateBank
from .trigger_list import TriggerList
from .utils import filter_dataframe, get_combined_snr, read_hdf_file

logging.basicConfig(level=logging.INFO)

DETECTOR_1 = "detector_1"
DETECTOR_2 = "detector_2"
IFAR = "ifar"
STAT = "stat"
TIMESLIDE_ID = "timeslide_id"
TIMESLIDE_INTERVAL = "timeslide_interval"
TIMESLIDE_VALUE = "timeslide_value"
TEMPLATE_ID = "template_id"
TRIGGER_ID = "trigger_id{}"
SNR = "snr"
LABEL = "label"


class TriggerTable(object):
    """Store Trigger info."""

    def __init__(
        self,
        ifar: np.ndarray,
        stat: np.ndarray,
        timeslide_value: np.ndarray,
        ifo_trigger: dict,
        template_bank: TemplateBank,
        label: Optional[np.ndarray] = None,
        **kwargs,
    ):
        """Get trigger table data from statmap and trigger lists.

        :param ifar:
        :param stat:
        :param timeslide_value:
        :param ifo_trigger:
        :param template_bank:
        """
        self.ifar = ifar
        self.stat = stat
        self.timeslide_value = timeslide_value
        self.ifo_trigger = ifo_trigger
        self.snr = self.get_triggers_combined_snr()
        self.template_bank = template_bank
        if label is None:
            self.label = [np.nan for _ in range(len(self.snr))]
        else:
            self.label = label
        logging.debug(f"Loaded table length {len(self.dataframe)}")

    @classmethod
    def from_csv(cls, trigger_table_csv: str):
        """Generate trigger table from CSV.

        :param trigger_table_csv:
        :return:
        """
        logging.info("Generating trigger table from {}".format(trigger_table_csv))
        dataframe = pd.read_csv(trigger_table_csv)
        return cls.from_dataframe(dataframe)

    @classmethod
    def from_dataframe(cls, trigger_table_dataframe: pd.DataFrame):
        """Generate trigger table from dataframe.

        :param trigger_table_dataframe:
        :return:
        """
        template_bank = TemplateBank.from_dataframe(trigger_table_dataframe)
        ifo_trigger = {
            "H1": TriggerList.from_dataframe(trigger_table_dataframe, "H1"),
            "L1": TriggerList.from_dataframe(trigger_table_dataframe, "L1"),
        }
        if LABEL in trigger_table_dataframe:
            label = trigger_table_dataframe[LABEL]
        else:
            label = [np.nan for _ in range(len(trigger_table_dataframe))]
        return cls(
            ifar=trigger_table_dataframe[IFAR],
            stat=trigger_table_dataframe[STAT],
            timeslide_value=trigger_table_dataframe[TIMESLIDE_VALUE],
            template_bank=template_bank,
            ifo_trigger=ifo_trigger,
            label=label,
        )

    @classmethod
    def from_pycbc_hdf_files(
        cls,
        trigger_table_file: str,
        table_name: str,
        bank_file: str,
        trigger_files: dict,
    ):
        """Generate trigger table from pyCBC hdf files.

        :param trigger_table_file:
        :param table_name:
        :param bank_file:
        :param trigger_files:
        :return:
        """
        logging.info("Reading {}".format(trigger_table_file))
        file_data = read_hdf_file(trigger_table_file)
        table_data = file_data.get(table_name)

        # Getting trigger statistics
        ifar = table_data.get(IFAR)[:]
        stat = table_data.get(STAT)[:]

        # Getting timeslide value
        timeslide_interval = file_data.attrs.get(TIMESLIDE_INTERVAL)
        timeslide_value = table_data.get(TIMESLIDE_ID)[:] * timeslide_interval

        # Get template bank and triggers
        template_bank = TemplateBank.from_pycbc_hdf_bank_file(
            bank_file=bank_file, id_num=table_data.get(TEMPLATE_ID)[:]
        )
        ifo_trigger = cls.get_trigger_list(file_data, trigger_files, table_data)

        logging.info("Returning Trigger Table")
        return cls(
            ifar=ifar,
            stat=stat,
            timeslide_value=timeslide_value,
            template_bank=template_bank,
            ifo_trigger=ifo_trigger,
        )

    @staticmethod
    def get_trigger_list(file_data, trigger_files, table_data):
        """Get the IFO trigger information from each IFO's trigger file.

        :param file_data:
        :param trigger_files:
        :param table_data:
        :return:
        """
        detector_labels = {
            file_data.attrs.get(DETECTOR_1).decode(): 1,
            file_data.attrs.get(DETECTOR_2).decode(): 2,
        }
        ifo_trigger = {ifo_name: None for ifo_name in detector_labels.keys()}
        for ifo_name, trigger_file in trigger_files.items():
            trigger_idx = detector_labels[ifo_name]
            trigger_id = table_data.get(TRIGGER_ID.format(trigger_idx))[:]
            trigger_list = TriggerList.from_pycbc_hdf_trigger_merge_file(
                trigger_merge_file=trigger_file, id_num=trigger_id
            )
            ifo_trigger.update({ifo_name: trigger_list})
        return ifo_trigger

    def to_dict(self):
        """Convert trigger table to dict.

        :return:
        """
        trigger_table_dict = {
            IFAR: self.ifar,
            STAT: self.stat,
            SNR: self.snr,
            TIMESLIDE_VALUE: self.timeslide_value,
            LABEL: self.label,
        }

        for ifo_name, trigger_list in self.ifo_trigger.items():
            trigger_table_dict.update(trigger_list.to_dict())

        trigger_table_dict.update(self.template_bank.to_dict())

        return trigger_table_dict

    def __to_dataframe(self):
        """Convert Trigger table to a dataframe.

        :return:
        """
        return pd.DataFrame.from_dict(self.to_dict())

    __dataframe = None

    @property
    def dataframe(self):
        """Trigger table in a dataframe format.

        :return:
        """
        if not isinstance(self.__dataframe, pd.DataFrame):
            self.__dataframe = self.__to_dataframe()
        return self.__dataframe

    @dataframe.setter
    def dataframe(self, dataframe: pd.DataFrame):
        """Set the trigger table's dataframe.

        :param dataframe:
        :return:
        """
        self.__dataframe = dataframe

    def to_csv(self, path):
        """Save trigger table as a csv.

        :param path:
        :return:
        """
        logging.info(f"Saving trigger table to {path}")
        dataframe_dir = os.path.dirname(path)
        if not os.path.isdir(dataframe_dir) and dataframe_dir != "":
            os.makedirs(dataframe_dir, exist_ok=True)
        self.dataframe.to_csv(path)

    def get_triggers_combined_snr(self):
        """Calculate combined SNR.

        :return:
        """
        try:
            snr_list = np.array(
                [trigger.snr.values for trigger in self.ifo_trigger.values()]
            )
        except AttributeError:
            # trigger.snr.values is an numpy.ndarray
            snr_list = [trigger.snr for trigger in self.ifo_trigger.values()]
        return get_combined_snr(snr_list)

    def add_catalogue_events(self, observing_run, chunk):
        """Add catalogue event data (GPS Start time etc).

        Note: This needs to be manually called (not called anywhere by pipeline).

        :param observing_run:
        :param chunk:
        :return:
        """
        catalogue_events = CatalogueEvents.get_chunk_events(
            observing_run=observing_run, chunk_number=chunk
        )
        catalogue_events_df = catalogue_events.to_trigger_table_df()

        original_count = len(self.dataframe)
        self.dataframe = self.dataframe.append(catalogue_events_df)
        new_count = len(self.dataframe)

        # logging
        catalogue_event_labels = list(catalogue_events_df.label.values)
        logging.info(
            f"Added {catalogue_event_labels} to trigger table list. "
            f"{original_count} Triggers --> {new_count} Triggers. "
        )

    def get_filtered_table(self, **kwargs):
        """Filter and return a new trigger table based on filtering kwargs passed.

        Parameters
        ----------
        kwargs: the key of the dataframe followed by '_min' or '_max' and the value
        that the key should be set to. For example,

        filtered_table = table.get_filtered_table(
            ifar_min=0.00002,
            snr_min=6.8,
            snr_max=8)

        Note: here 'ifar', 'snr' are column names of the dataframe. If not an error
        will be thrown

        Returns
        -------
        The filtered table

        """
        dataframe = self.dataframe
        logging.info("{} triggers before filtering".format(len(dataframe)))
        dataframe = filter_dataframe(dataframe, **kwargs)
        logging.info("{} triggers after filtering".format(len(dataframe)))
        return self.from_dataframe(dataframe)
