"""Module to retrieve data from chunks."""
import json
import logging
import os
from pprint import pformat
from typing import Dict, List, Optional

import pandas as pd
from astropy.time import Time

from .injection_parser import InjectionTriggerTable as InjTrgTable
from .trigger_table import TemplateBank, TriggerTable
from .trigger_types import BACKGROUND, FOREGROUND, INJECTION, TRIGGER_TYPES

CURRENT_PATH = os.path.dirname(__file__)

data_path = dict(
    O2=os.path.join(CURRENT_PATH, "data/O2_data.json"),
    O3=os.path.join(CURRENT_PATH, "data/O3_data.json"),
)

CHUNK_NUMBER = "chunk_number"
START_TIME = "start_time"
END_TIME = "end_time"
ROOT = "root"
STAT_MAP = "stat_map"
H1_TRIGGER_MERGE = "h1_trigger_merge"
L1_TRIGGER_MERGE = "l1_trigger_merge"
BANK = "bank"

INJ_STAT_MAP = "inj_stat_map"
H1_INJ_TRIGGER_MERGE = "h1_inj_trigger_merge"
L1_INJ_TRIGGER_MERGE = "l1_inj_trigger_merge"
INJECTION_SEARCH = "injection_search"
INJECTION_FILE = "injection_file"

DICT_COLUMNS = [
    INJECTION_FILE,
    INJECTION_SEARCH,
    INJ_STAT_MAP,
    H1_INJ_TRIGGER_MERGE,
    L1_INJ_TRIGGER_MERGE,
]


class ChunkData:
    """Stores paths to data and can create trigger tables and banks from the paths."""

    def __init__(
        self,
        observing_run: int,
        number: int,
        time: List[int],
        bank: str,
        statmap: str,
        trigger_dict: Dict[str, str],
        table_name: str,
        trigger_type: str,
        injection_file: Optional[str] = None,
    ):
        """
        Create a ChunkData obj.

        :param observing_run: observing run number
        :param number: chunk number
        :param time: time[0] chunk start time, time[1] is chunk end time
        :param bank: path to bank file
        :param statmap: path to statmap file
        :param trigger_dict: dict of ifos with paths to their merge files
        :param trigger_type: the type of trigger (foreground/background)
        :param injection_file: the file with the injection's true vals
        """
        self.observing_run = observing_run
        self.number = number
        self.start_time = time[0]
        self.end_time = time[1]
        self.bank_path = bank
        self.statmap_path = statmap
        self.trigger_merge_dict = trigger_dict
        self.table_name = table_name
        self.trigger_type = trigger_type
        if injection_file:
            self.injection_file_path = injection_file

    @property
    def trigger_table(self):
        """Trigger table using ChunkData's paths.

        :return: TriggerTable
        """
        if not hasattr(self, "__trigger_table"):
            table_kwargs = dict(
                trigger_table_file=self.statmap_path,
                table_name=self.table_name,
                bank_file=self.bank_path,
                trigger_files=self.trigger_merge_dict,
            )
            logging.info(f"Getting table from info from {pformat(table_kwargs)}")
            if self.trigger_type == INJECTION:
                self.__trigger_table = InjTrgTable.from_pycbc_hdf_files(
                    **table_kwargs, injection_file=self.injection_file_path
                )
            else:
                self.__trigger_table = TriggerTable.from_pycbc_hdf_files(**table_kwargs)
        return self.__trigger_table

    @trigger_table.setter
    def trigger_table(self, table):
        self.__trigger_table = table

    @property
    def bank(self):
        """Create a template bank from ChunkData's paths.

        :return: TemplateBank
        """
        if not hasattr(self, "__bank"):
            self.__bank = TemplateBank.from_pycbc_hdf_bank_file(self.bank_path)
        return self.__bank

    @staticmethod
    def _get_chunk_row(observing_run_filename: str, chunk_number: int):
        with open(observing_run_filename) as json_file:
            chunk_data = json.load(json_file)[f"{chunk_number}"]
        return chunk_data

    @classmethod
    def from_json(
        cls,
        observing_run: int,
        chunk_number: int,
        trigger_type: str,
        injection_key: Optional[str] = None,
        injection_waveform: Optional[str] = None,
    ):
        """Generate chunk data from csv of observing run files.

        :param observing_run:
        :param chunk_number:
        :param trigger_type:
        :param injection_key
        :param injection_waveform
        :return:
        """
        chunk_data = cls._get_chunk_row(data_path[f"O{observing_run}"], chunk_number)

        injection_file = None
        if trigger_type == INJECTION:
            assert injection_key is not None, "inj_key (BBH, BNS, etc) required"
            assert injection_waveform is not None, "inj_waveform (IMR, etc) required"
            for dict_column in DICT_COLUMNS:
                data = chunk_data[dict_column][injection_key]
                waveforms = data.keys()
                if injection_waveform in waveforms:
                    chunk_data[dict_column] = data[injection_waveform]
                else:
                    raise KeyError(f"{injection_waveform} not present in {waveforms}.")
            injection_file = os.path.join(chunk_data[ROOT], chunk_data[INJECTION_FILE])

        statmap, trigger_dict = cls.get_trigger_data(chunk_data, trigger_type)

        data_kwargs = dict(
            observing_run=observing_run,
            number=chunk_number,
            time=[chunk_data[START_TIME], chunk_data[END_TIME]],
            bank=os.path.join(chunk_data[ROOT], chunk_data[BANK]),
            statmap=statmap,
            trigger_dict=trigger_dict,
            trigger_type=trigger_type,
            table_name=chunk_data[f"{trigger_type}_type"],
            injection_file=injection_file,
        )
        logging.info(f"Creating chunk object:\n{pformat(data_kwargs)}")
        return cls(**data_kwargs)

    @staticmethod
    def get_trigger_data(chunk_data: pd.DataFrame, trigger_type: str):
        """Unpack paths from a chunk data series.

        :param chunk_data: dataframe with all chunk paths
        :param trigger_type: the type of data paths to get.
        Valid types ['foreground', 'background', 'injection']
        :return:
        """
        root_path = chunk_data[ROOT]
        statmap, trigger_merge_dict = None, None

        if trigger_type not in TRIGGER_TYPES:
            raise ValueError(
                f"{trigger_type} is an invalid trigger_type. "
                f"Valid types are {TRIGGER_TYPES}"
            )
        elif trigger_type in [FOREGROUND, BACKGROUND]:
            statmap = os.path.join(root_path, chunk_data[STAT_MAP])
            trigger_merge_dict = dict(
                H1=os.path.join(root_path, chunk_data[H1_TRIGGER_MERGE]),
                L1=os.path.join(root_path, chunk_data[L1_TRIGGER_MERGE]),
            )
        elif trigger_type == INJECTION:
            statmap = os.path.join(root_path, chunk_data[INJECTION_SEARCH])
            trigger_merge_dict = dict(
                H1=os.path.join(root_path, chunk_data[H1_INJ_TRIGGER_MERGE]),
                L1=os.path.join(root_path, chunk_data[L1_INJ_TRIGGER_MERGE]),
            )
        return statmap, trigger_merge_dict


def get_bank(observing_run: int, chunk_number: int):
    """Given the observing run and chunk number, a bank is returned."""
    observing_run_code = f"O{observing_run}"
    logging.info(f"Getting bank for {observing_run_code} chunk {chunk_number}")
    observing_run_filename = data_path[observing_run_code]
    chunk_data = ChunkData._get_chunk_row(observing_run_filename, chunk_number)
    chunk_bank = os.path.join(chunk_data[ROOT], chunk_data[BANK])
    bank = TemplateBank.from_pycbc_hdf_bank_file(chunk_bank)
    return bank


def get_trigger_data(
    observing_run: int,
    chunk_number: int,
    trigger_type: str,
    injection_key: Optional[str] = None,
    injection_waveform: Optional[str] = None,
):
    """Get data from an observing run and chunk number for a specific trigger type.

    :param observing_run: 2 or 3
    :param chunk_number: from 1 to X (depends on observing run number)
    :param trigger_type: foreground, background or injection
    :return: ChunkData
    """
    run_code = f"O{observing_run} Chunk {chunk_number}"
    kwargs = dict(
        observing_run=observing_run,
        chunk_number=chunk_number,
        trigger_type=trigger_type,
        injection_key=injection_key,
        injection_waveform=injection_waveform,
    )
    logging.info(f"Getting trigger data for {run_code}:\n{pformat(kwargs)}")
    my_chunk_data = ChunkData.from_json(**kwargs)
    return my_chunk_data


def get_chunk_number_durations() -> pd.DataFrame:
    obeserving_run_dicts = []
    with open(data_path["O2"]) as json_file:
        obeserving_run_data = json.load(json_file)
        for chunk_dict in obeserving_run_data.values():
            start = Time(chunk_dict["start_time"], format="gps").to_datetime()
            end = Time(chunk_dict["end_time"], format="gps").to_datetime()
            obeserving_run_dicts.append(
                {
                    "chunk_number": chunk_dict["chunk_number"],
                    "start_time": chunk_dict["start_time"],
                    "start_stamp": f"{str(start.year)}-{start.month:02}-{start.day:02}",
                    "end_time": chunk_dict["end_time"],
                    "end_stamp": f"{str(end.year)}-{end.month:02}-{end.day:02}",
                }
            )
    df = pd.DataFrame(obeserving_run_dicts)
    return df


def get_trigger_chunk_number(gps_time: float) -> int:
    """Returns the chunk number of a GPS time"""
    df = get_chunk_number_durations()
    df = df[df["start_time"] <= gps_time]
    df = df[gps_time <= df["end_time"]]
    if len(df) != 1:
        raise ValueError(f"{gps_time} is not in O2")
    return int(df["chunk_number"])
