# -*- coding: utf-8 -*-
"""Module to load catalogue events."""
import os

import pandas as pd
from numpy import float64, int64

DELTA_T = 2

CATALOGUE_CSV = "data/catalogue_events.csv"
CATALOGUE = "Catalog"
OBSERVING_RUN = "Observing Run"
PYCBC_CHUNK = "PyCBC Chunk"
GPS_START_TIME = "GPS start time"
TRIGGER_TIME = "Trigger Time"
EVENT = "Event"

# DTYPE = {
#     "Event": str,
#     "Catalog": str,
#     "PyCBC Pbbh": float64,
#     "GstLAL Pbbh": float64,
#     "cWB Pbbh": float64,
#     "IAS Pbbh": float64,
#     "BCR Pastro": float64,
#     "msource (M)": str,
#     "χeff": str,
#     "Observing Run": int64,
#     "PyCBC Chunk": int64,
#     "Trigger Time": float64
# }


class CatalogueEvents:
    """Class to process catalogue events."""

    _data = None

    @property
    def data(self) -> pd.DataFrame:
        """Get catalogue data."""
        if not isinstance(self._data, pd.DataFrame):
            file_dir = os.path.dirname(os.path.abspath(__file__))
            catalogue_path = os.path.join(file_dir, CATALOGUE_CSV)
            df = pd.read_csv(catalogue_path, sep=",", index_col=None)
            df[GPS_START_TIME] = float64(df[TRIGGER_TIME]) - DELTA_T
            self._data = CatalogueEvents._sort_dataframe_by_gps_time(df)
        return self._data

    @data.setter
    def data(self, catalogue_df):
        """Set Catalogue data."""
        for header in [EVENT, GPS_START_TIME, CATALOGUE, OBSERVING_RUN]:
            assert header in list(catalogue_df.columns.values)
        self._data = catalogue_df

    @property
    def catalogue_sources(self):
        """Get list of catalouges eg IAS-1, GWTC1, etc."""
        return list(set(self.data[CATALOGUE]))

    @staticmethod
    def _sort_dataframe_by_gps_time(df):
        """Sort a dataframe by gps time."""
        return df.sort_values(by=GPS_START_TIME)

    def _filter_events(self, search_col, search_val, catalogue=pd.DataFrame()):
        """Filter dataframe by the search val in a search col."""
        if catalogue.empty:
            catalogue = self.data
        filtered_df = catalogue[catalogue[search_col] == search_val]
        return self._sort_dataframe_by_gps_time(filtered_df)

    @classmethod
    def _get_specific_cat(cls, cat_regex) -> pd.DataFrame:
        """Filter dataframe based on cataluge name regex."""
        catalogue = cls()
        cat_labels = [c for c in catalogue.catalogue_sources if cat_regex in c]
        df = pd.DataFrame()
        for cat_label in cat_labels:
            df = df.append(catalogue._filter_events(CATALOGUE, cat_label))
        catalogue.data = catalogue._sort_dataframe_by_gps_time(df)
        return catalogue

    @classmethod
    def get_ias_events(cls):
        """Get IAS catalogue."""
        return cls._get_specific_cat("IAS")

    @classmethod
    def get_gwtc_events(cls):
        """Get GWTC catalogue list."""
        return cls._get_specific_cat("GWTC")

    @classmethod
    def get_chunk_events(cls, observing_run: int, chunk_number: int):
        """Get Chunk catalouge."""
        cat = cls()
        df = cat._filter_events(OBSERVING_RUN, observing_run)
        df = cat._filter_events(PYCBC_CHUNK, chunk_number, df)
        cat.data = cat._sort_dataframe_by_gps_time(df)
        return cat

    def to_trigger_table_df(self):
        """Convert catalogue to a trigger dataframe format."""
        return pd.DataFrame(
            dict(
                label=self.data[EVENT],
                H1_start_time=self.data[GPS_START_TIME],
                H1_trigger_time=self.data[GPS_START_TIME] + 2,
                L1_start_time=self.data[GPS_START_TIME],
                L1_trigger_time=self.data[GPS_START_TIME] + 2,
            )
        )


if __name__ == "__main__":
    pd.set_option("display.max_columns", None)
    pd.set_option("display.width", 1000)
    pd.set_option('display.float_format', lambda x: '%.5f' % x)
    print(CatalogueEvents().data)
