"""PyCBC Template bank module."""

import logging
import os

import numpy as np
import pandas as pd
from bilby.gw.conversion import (
    component_masses_to_chirp_mass,
    component_masses_to_mass_ratio,
    component_masses_to_total_mass,
)
from tqdm import tqdm

from .utils import correct_m1_m2, filter_dataframe, read_hdf_file

logging.basicConfig(level=logging.INFO)

MASS1 = "mass1"
MASS2 = "mass2"
MASS_1 = "mass_1"
MASS_2 = "mass_2"
SPIN1Z = "spin1z"
SPIN2Z = "spin2z"
F_LOWER = "f_lower"
TEMPLATE_ID = "template_id"
TOTAL_MASS = "mass_total"
CHIRP_MASS = "mass_chirp"
MASS_RATIO = "mass_ratio"
TEMPLATE_HASH = "template_hash"


class TemplateBank(object):
    """Stores info on the template bank."""

    def __init__(
        self,
        f_lower,
        mass_1,
        mass_2,
        mass_ratio,
        mass_chirp,
        mass_total,
        spin1z,
        spin2z,
        id_num,
    ):
        """Template bank.

        :param f_lower:
        :param mass_1:
        :param mass_2:
        :param mass_ratio:
        :param mass_chirp:
        :param mass_total:
        :param spin1z:
        :param spin2z:
        :param id_num: if provided, returns this one template's value
        """
        self.f_lower = f_lower
        self.mass_1 = mass_1
        self.mass_2 = mass_2
        self.spin1z = spin1z
        self.spin2z = spin2z
        self.id_num = id_num
        # degenerate param used for constraining triggers
        self.mass_ratio = mass_ratio
        self.mass_chirp = mass_chirp
        self.mass_total = mass_total

    @property
    def dataframe(self):
        """Trigger table in a dataframe format.

        :return:
        """
        if not hasattr(self, "__dataframe"):
            self.__dataframe = self.__to_dataframe()
        return self.__dataframe

    @dataframe.setter
    def dataframe(self, dataframe: pd.DataFrame):
        """Set the trigger table's dataframe.

        :param dataframe:
        :return:
        """
        self.__dataframe = dataframe

    @classmethod
    def from_pycbc_hdf_bank_file(cls, bank_file, id_num=None):
        """Create template bank from pycbc hdf.

        TEMPLATE BANK:
        /                        Group
        /f_lower                 Dataset {428725}
        /mass1                   Dataset {428725}
        /mass2                   Dataset {428725}
        /spin1z                  Dataset {428725}
        /spin2z                  Dataset {428725}
        /template_hash           Dataset {428725}

        :param bank_file:
        :param id_num:
        :return:
        """
        logging.info("Reading {}".format(bank_file))
        bank_file = read_hdf_file(bank_file)

        if id_num is None:
            f_lower = bank_file.get(F_LOWER)[()]
            mass_1 = bank_file.get(MASS1)[()]
            mass_2 = bank_file.get(MASS2)[()]
            spin1z = bank_file.get(SPIN1Z)[()]
            spin2z = bank_file.get(SPIN2Z)[()]

            # the id inside the STAMAP is the index number of template
            id_num = [i for i in range(len(mass_1))]

        else:
            # If there are id numbers provided, extract those specific templates
            f_lower = np.take(bank_file.get(F_LOWER, default=None), indices=id_num)
            mass_1 = np.take(bank_file.get(MASS1), indices=id_num)
            mass_2 = np.take(bank_file.get(MASS2), indices=id_num)
            spin1z = np.take(bank_file.get(SPIN1Z), indices=id_num)
            spin2z = np.take(bank_file.get(SPIN2Z), indices=id_num)

        # making sure m1 > m2
        if hasattr(mass_1, "__len__"):
            for idx in tqdm(range(len(mass_1))):
                mass_1[idx], mass_2[idx] = correct_m1_m2(mass_1[idx], mass_2[idx])
        else:
            mass_1, mass_2 = correct_m1_m2(mass_1, mass_2)

        logging.info("Calculating chirp mass, total mass and mass ratio")
        mass_chirp = np.vectorize(component_masses_to_chirp_mass)(mass_1, mass_2)
        mass_total = np.vectorize(component_masses_to_total_mass)(mass_1, mass_2)
        mass_ratio = np.vectorize(component_masses_to_mass_ratio)(mass_1, mass_2)

        logging.info("Generating bank data")
        return cls(
            f_lower=f_lower,
            mass_1=mass_1,
            mass_2=mass_2,
            mass_chirp=mass_chirp,
            mass_total=mass_total,
            mass_ratio=mass_ratio,
            spin1z=spin1z,
            spin2z=spin2z,
            id_num=id_num,
        )

    def to_dict(self):
        """Convert bank to a dictionary.

        :return:
        """
        return {
            MASS_1: self.mass_1,
            MASS_2: self.mass_2,
            TOTAL_MASS: self.mass_total,
            CHIRP_MASS: self.mass_chirp,
            MASS_RATIO: self.mass_ratio,
            SPIN1Z: self.spin1z,
            SPIN2Z: self.spin2z,
            F_LOWER: self.f_lower,
            TEMPLATE_ID: self.id_num,
        }

    def __to_dataframe(self):
        """Convert bank to a df.

        :return:
        """
        return pd.DataFrame.from_dict(self.to_dict())

    def to_csv(self, path):
        """Save bank as csv.

        :param path:
        :return:
        """
        logging.info("saving template bank info to {}".format(path))
        dataframe_dir = os.path.dirname(path)
        if not os.path.isdir(dataframe_dir):
            os.makedirs(dataframe_dir, exist_ok=True)
        self.__to_dataframe().to_csv(path)

    def get_filtered_bank(self, **kwargs):
        """Filter and return a new template bank based on filtering kwargs passed.

        Parameters
        ----------
        kwargs: the key of the dataframe followed by '_min' or '_max' and the value
        that the key should be set to. For example,

        filtered_template_bank = template_bank.get_filtered_bank(
            mass_1_min=0.00002,
            mass_2_min=6.8,
            mass_chirp_max=8)

        Note: here 'mass_1', 'mass_2' are column names of the dataframe. If not an error
        will be thrown

        Returns
        -------
        The filtered template bank

        """
        dataframe = self.__to_dataframe()
        dataframe = filter_dataframe(dataframe, **kwargs)
        return self.from_dataframe(dataframe)

    @classmethod
    def from_dataframe(cls, bank_dataframe):
        """Generate bank from dataframe.

        :param bank_dataframe:
        :return:
        """
        return cls(
            f_lower=bank_dataframe[F_LOWER],
            mass_1=bank_dataframe[MASS_1],
            mass_2=bank_dataframe[MASS_2],
            mass_chirp=bank_dataframe[CHIRP_MASS],
            mass_total=bank_dataframe[TOTAL_MASS],
            mass_ratio=bank_dataframe[MASS_RATIO],
            spin1z=bank_dataframe[SPIN1Z],
            spin2z=bank_dataframe[SPIN1Z],
            id_num=bank_dataframe[TEMPLATE_ID],
        )

    @classmethod
    def from_csv(cls, csv):
        """Generate bank from csv.

        :param csv:
        :return:
        """
        return cls.from_dataframe(pd.read_csv(csv))
