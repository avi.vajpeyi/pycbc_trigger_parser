"""Trigger_table_webpage.

This module contains a class TriggerPage that inherits BilbyResultsPage from
bilby_report to help make an HTML page summarising the data about some triggers
"""

import logging
import os
from typing import Optional

import matplotlib
from bilby_report import DataframeTemplate, IFrameTemplate, ImageTemplate
from bilby_report.bilby_results_summary_page import BilbyResultsPage
from bilby_report.tools import plotting

from . import TemplateBank, TriggerTable, TriggerTableTimes
from .plotting import plot_template_bank
from .plotting.settings import get_plot_marker_kwargs

matplotlib.use("Agg")
logging.basicConfig(level=logging.INFO)


class TriggerPage(BilbyResultsPage):
    """Generate webpage of trigger info."""

    def __init__(
        self,
        webdir: str,
        trigger_csv: str,
        title: Optional[str] = "O3 Chunk6 Triggers",
        bank_file_path=None,
        start_time=None,
        end_time=None,
    ):
        """Collect webpage data.

        :param webdir:
        :param trigger_csv:
        :param title:
        :param bank_file_path:
        :param start_time:
        :param end_time:
        """
        self.start_time = start_time
        self.end_time = end_time
        self.bank_csv = bank_file_path
        self.webdir_content_dir_name = "O3_Triggers"
        self.create_from_scratch = True
        self.title = title
        self.webdir = webdir

        # loading data
        self.trigger_table = TriggerTable.from_csv(trigger_csv)
        self.times_table = TriggerTableTimes(self.trigger_table)

    def get_sections(self):
        """Define different sections for webpage.

        :return:
        """
        sections = []

        trigger_df = self.trigger_table.dataframe
        times_df = self.times_table.to_dataframe()

        try:

            logging.info("[PAGE-PLOTTING]: Generating trigger search parameter table")
            sections.append(
                DataframeTemplate(dataframe=trigger_df, title="Triggers list")
            )
        except Exception as e:
            logging.warning("Skipping plotting of Trigger table: {}".format(e))

        try:
            logging.info("[PAGE-PLOTTING]: Generating trigger times table")
            sections.append(DataframeTemplate(dataframe=times_df, title="", text=""))

        except Exception as e:
            logging.warning("Skipping plotting of Trigger times: {}".format(e))

        try:
            logging.info("[PAGE-PLOTTING]: Generating trigger times plot")
            df = times_df
            df["H1_t0"] = df["H1_timeslid_time"]
            df["H1_t1"] = df["H1_timeslid_time_end"]
            df["L1_t0"] = df["L1_timeslid_time"]
            df["L1_t1"] = df["L1_timeslid_time_end"]
            logging.debug(df)

            times_path = plotting.plot_times(
                times_dataframe=df,
                start_time=self.start_time,
                end_time=self.end_time,
                filename=os.path.join(self.webdir, "times.html"),
            )
            sections.append(
                IFrameTemplate(
                    title="",
                    content_path=times_path,
                    height="400",
                    width="100%",
                    save_dir=self.webdir,
                )
            )

        except Exception as e:
            logging.warning("Skipping plotting of times: {}".format(e))

        if self.bank_csv:
            logging.info("[PAGE-PLOTTING]: Generating bank plot")
            try:
                template_bank = TemplateBank.from_csv(self.bank_csv)
                bank_kwargs = dict(
                    color="plum", s=0.5, marker=".", alpha=0.1, label="Template Bank"
                )
                trigger_kwargs = dict(
                    color="black", s=15, marker="o", alpha=0.5, label="Triggers"
                )
                plot_template_bank(
                    scatter_points=[
                        dict(
                            data=template_bank.dataframe,
                            plot_kwargs=dict(**bank_kwargs),
                        ),
                        dict(
                            data=self.trigger_table.dataframe,
                            plot_kwargs=dict(**trigger_kwargs),
                        ),
                    ],
                    web_dir=self.webdir,
                )

                bank_image_path = os.path.join(self.webdir, "template_bank_masses.png")

                sections.append(
                    ImageTemplate(
                        title="Template Bank",
                        img_path=bank_image_path,
                        width="100%",
                        save_dir=self.webdir,
                    )
                )
            except Exception as e:
                logging.warning("Skipping plotting of template bank: {}".format(e))

        try:
            logging.info("[PAGE-PLOTTING]: Generating far vs stat plot")
            plot_data = [
                dict(
                    far=1 / trigger_df["ifar"],
                    rank=trigger_df["stat"],
                    plot_kwargs=get_plot_marker_kwargs()["background"],
                )
            ]
            far_plot = plotting.plot_far_vs_rank(
                plot_data=plot_data,
                rank_x_label=r"PyCBC Ranking Statistic",
                log_scale=False,
                title="",
                filename=os.path.join(self.webdir, "far_vs_stat.png"),
            )
            sections.append(
                ImageTemplate(
                    title="Trigger False Alarm Rates",
                    img_path=far_plot,
                    width="100%",
                    save_dir=self.webdir,
                )
            )
        except Exception as e:
            logging.warning(f"Skipping plotting of far: {e}")

        try:
            logging.info("[PAGE-PLOTTING]: Generating mass scatter plot")
            mass_scatter_path = plotting.plot_mass_scatter(
                trigger_df, filename=os.path.join(self.webdir, "mass_scatter.html")
            )
            sections.append(
                IFrameTemplate(
                    title="Trigger Masses",
                    content_path=mass_scatter_path,
                    height="500",
                    save_dir=self.webdir,
                )
            )
        except Exception as e:
            logging.warning("Skipping plotting of masses: {}".format(e))

        try:
            logging.info("[PAGE-PLOTTING]: Generating mass distribution plot")
            mass_distribution_path = plotting.plot_dataframe_coloumn_histograms(
                trigger_df,
                plot_key_to_label=dict(
                    mass_ratio="q", mass_total="M", mass_chirp="Chirp Mass"
                ),
                filename=os.path.join(self.webdir, "mass_distribution.html"),
            )
            sections.append(
                IFrameTemplate(
                    title="",
                    content_path=mass_distribution_path,
                    height="500",
                    save_dir=self.webdir,
                )
            )
        except Exception as e:
            logging.warning("Skipping plotting of distributed masses: {}".format(e))

        try:
            logging.info("[PAGE-PLOTTING]: Generating search analysis stats plot")
            analysis_stats_path = plotting.plot_dataframe_coloumn_histograms(
                trigger_df,
                plot_key_to_label=dict(snr="SNR", ifar="iFAR", stat="STAT"),
                filename=os.path.join(self.webdir, "search_histograms.html"),
            )
            sections.append(
                IFrameTemplate(
                    title="Search Statistics",
                    content_path=analysis_stats_path,
                    height="500",
                    save_dir=self.webdir,
                )
            )
        except Exception as e:
            logging.warning("Skipping plotting of PE stats: {}".format(e))

        return sections
