<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Background triggers for O3](#background-triggers-for-o3)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Catalogue Event Data source

## [IAS Events from O2](https://arxiv.org/pdf/1904.07214.pdf)
* 



# Background triggers for O3

Page with the O3 dirs:
https://wiki.ligo.org/CBC/Searches/PycbcC00HlRunSchedule

https://ldas-jobs.ligo.caltech.edu/~tdent/o3/runs/hl/c00/a6_initial/

Thomas Dent and TJ Massinger have provided the path to the dir with the
preliminary pyCBC background trigger analysis for O3's chunk 6 (GPS time
1241725020-1242485126), which contains
S190521g (GPS time 1242442967).

```
/home/tdent/o3/analysis/pycbc/c00/hl/a6_gating_1/output/full_data/
```
This dir has ~5653 files, one of which is
`*STATMAP_FULL_DATA_FULL_VETOES*.hdf`
    *   `H1L1-STATMAP_FULL_DATA_FULL_VETOES-1241724868-760282.hdf`.
This file acts as a "map" to the
* `*TRIGGER_MERGE*.hdf` merge files in the same directory
    * `H1-HDF_TRIGGER_MERGE_FULL_DATA-1241724868-760282.hdf`
    * `L1-HDF_TRIGGER_MERGE_FULL_DATA-1241724868-760282.hdf`
* `BANK` file in the `../bank directory` (not the split bank files).
    * `../bank/H1L1-BANK2HDF-1241724868-760282.hdf`
The structure and descriptions of attribute of this file can be
viewed [here](../../statmap_structure.md).
