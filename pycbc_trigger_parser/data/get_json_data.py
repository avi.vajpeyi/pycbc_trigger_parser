"""
Converts and updates CSV Data to JSON data.

Uses original csv data for chunks and fetches injection files for each. Then
writes each to JSON format.
"""

import json
import os

import pandas as pd

allowed_dirs = ["bank", "full_data", "_INJ_", "inj_files"]

allowed_files = ["STATMAP", "TRIGGER_MERGE", "BANK2HDF", "HDFINJFIND", ".xml"]

ignored_files = ["SPLITBANK", "STRIP", "INJECTIONS", "COMBINE"]

ignored_dirs = ["results"]


sub_types = {
    "BBH": ["SEOBNRV3", "SEOBNRV4", "IMRPHENOMD"],
    "IMBH": ["EOBNRV2HM", "SEOBNRV3", "SEOBNRV4", "IMRPHENOMD"],
    "NSBH": ["SEOBNRV3", "SEOBNRV4", "IMRPHENOMD", "STT4"],
    "BNS": ["STT2", "STT4"],
}

allowed_fstrs = {
    # try full data first- will work in Python 3 as keys are alphabetical
    "H1-HDF_TRIGGER_MERGE_FULL_DATA": "h1_trigger_merge",
    "L1-HDF_TRIGGER_MERGE_FULL_DATA": "l1_trigger_merge",
    "STATMAP_FULL_DATA": "stat_map",
    "STATMAP": ("inj_stat_map", sub_types),
    "H1-HDF_TRIGGER_MERGE": ("h1_inj_trigger_merge", sub_types),
    "L1-HDF_TRIGGER_MERGE": ("l1_inj_trigger_merge", sub_types),
    "BANK2HDF": "bank",
    "HDFINJFIND": ("injection_search", sub_types),
    ".xml": ("injection_file", sub_types),
}


def string_checker(s, allowed_substrings, not_allowed_substrings):
    """Check if the string s is valid.

    :param s: string that is being checked
    :param allowed_substrings: list of substrings (one of these must be in the string s)
    :param not_allowed_substrings: list of substrings (none of these can be in string s)
    :return: bool (True if valid string, else False)
    """
    valid_string = any(allowed in s for allowed in allowed_substrings)
    invalid_string = any(not_allowed in s for not_allowed in not_allowed_substrings)
    return valid_string and not invalid_string


def list_files(startpath):
    """List the relevant pyCBC files.

    :param startpath: start path of the search
    :return: None
    """
    print(f"Starting search from root: {startpath}")
    for root, dirs, files in os.walk(startpath):
        if string_checker(root, allowed_dirs, ignored_dirs):
            print(f"{root}/")
            for f in files:
                if string_checker(f, allowed_files, ignored_files):
                    subindent = " -- "
                    print(f"{subindent}{f}")
            print("\n")


def handle_file(f, dir_name, allowed_strings, json_dict):
    """
    For a given file, add to json dictionary with correct key.

    :param f: filename
    :param dir_name: current relative directory of f
    :param allowed_strings: types of strings allowed in given files
    :param json_dict: current dictionary to update for converting to JSON
    :return:  None
    """
    # recursively check each sub-level- really only for sub-dicts
    # check over given keys and subkeys
    for f_str, val in allowed_strings.items():
        if string_checker(f, [f_str], []):
            # go over all subfile types now
            # if value is not a dict then simply set to value
            if type(val) == str:
                json_dict[val] = os.path.join(dir_name, f)
            # otherwise handle recursive- max one level!
            elif type(val) == tuple:
                if val[0] not in json_dict.keys():
                    json_dict[val[0]] = {}
                handle_file(f, dir_name, allowed_strings[f_str][1], json_dict[val[0]])
            # if given a list just check each individual element
            elif type(val) == list:
                if f_str not in json_dict.keys():
                    json_dict[f_str] = {}
                for s in val:
                    if string_checker(f, [s], []):
                        json_dict[f_str][s] = os.path.join(dir_name, f)
    return


def get_json_dict(startpath):
    """
    Get total dictionary to convert to JSON.

    :param startpath: root path to start looking for files to include.
    :return: dictionary of items to convert to JSON
    """
    json_dict = {}
    print(f"Starting search from root: {startpath}")
    for root, dirs, files in os.walk(startpath):
        dir_name = os.path.basename(root)
        if string_checker(root, allowed_dirs, ignored_dirs):
            for f in files:
                if string_checker(f, allowed_files, ignored_files):
                    handle_file(f, dir_name, allowed_fstrs, json_dict)
    return json_dict


def parse_csv_dict(target_str):
    """Convert a string encoded as '{k1: v1, k2: v2}' into a dictionary."""
    return {i.split(":")[0]: i.split(":")[1] for i in (target_str[1:-1].split(", "))}


def convert_to_json(csv_file):
    """Convert CSV data into a python dictionary ready to be converted to JSON."""
    df = pd.read_csv(csv_file)
    # replace nans with None to correctly convert to JSON
    df = df.replace({pd.np.nan: None})
    # sort by chunk number and then convert to a dictionary by {index,
    # {'column, value'}}
    chunk_dict = df.set_index("chunk_number", drop=False).to_dict(orient="index")

    # keys which should be dictionary objects themselves in the new scheme and
    # that we thus need to parse.
    keys = [
        "injection_file",
        "inj_stat_map",
        "injection_search",
        "h1_inj_trigger_merge",
        "l1_inj_trigger_merge",
    ]
    for chunk in chunk_dict.values():
        for k in keys:
            # If it doesn't convert, just give it the empty dictionary as a
            # value I don't choose an exception in particular since in general
            # various exceptions happen when not faced with the perfect
            # syntax. Could be replaced by something cleaner.
            try:
                chunk[k] = parse_csv_dict(chunk[k])
            except Exception as e:
                if type(e) == TypeError or type(e) == IndexError:
                    chunk[k] = None
    return chunk_dict


def write_json(fname, data):
    """Write json data as dictionary to JSON format."""
    with open(fname, "w") as fout:
        json.dump(data, fout, indent=2)


def test_eq(dict1, dict2):
    """Checks if each key/value of dict1 is in dict2 (opposite not need be true)."""
    for k, v in dict1.items():
        if type(v) is dict:
            test_eq(v, dict2[k])
        # have second dict has a sub-dict that we need to un-pack:
        elif type(dict2[k]) is dict:
            # sometimes input data has some extra spaces...
            assert v.strip() == list(dict2[k].values())[0].strip()
        else:
            assert v == dict2[k]


def test_paths_valid(chunk_dict, root):
    """
    Check if each path in dictionary is valid.

    :param chunk_dict: dictionary to check (of a given data chunk).
    :param root: root directory of that chunk (used for recursion).
    """
    for k, v in chunk_dict.items():
        if type(v) == str:
            if "." in v:
                assert os.path.exists(os.path.join(root, v))
        elif type(v) == dict:
            test_paths_valid(v, root)


def get_update_convert_csv_data(f_csv, f_out, test_chunk_nums=[]):
    """
    Combine obtaining, updating, and converting csv data to JSON.

    :param f_csv: csv data file to update and convert
    :param f_out: output json filename
    :param test_chunk_nums: chunks to test if original and updated
                            injection data are equal.

    """
    data = convert_to_json(f_csv)
    for chunk in data.keys():
        if not (data[chunk]["root"]):
            continue
        root = data[chunk]["root"]
        data_updated = data[chunk].copy()
        data_updated.update(get_json_dict(root))
        # If a chunk already had data (e.g. 17th chunk of O2), test
        # to make sure it's the same
        for test_chunk in test_chunk_nums:
            if chunk == test_chunk:
                test_eq(data[chunk], data_updated)
        # now set equal
        data[chunk] = data_updated
        # make sure paths are valid
        test_paths_valid(data[chunk], data[chunk]["root"])
    write_json(f_out, data)


if __name__ == "__main__":
    # get, update, and convert O2 data to dict json format
    get_update_convert_csv_data("O2_data.csv", "O2_data.json", test_chunk_nums=[17])
    # same with O3- although we only have one chunk of data here.
    get_update_convert_csv_data("O3_data.csv", "O3_data.json", test_chunk_nums=[6])
