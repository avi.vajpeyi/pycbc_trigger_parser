<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Notes](#notes)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Notes
These images are the sources for the GPS trigger times of the various events

LIGO event catalogue:
https://doi.org/10.7935/82H3-HH23