"""This module stores the keys for the different trigger types."""
INJECTION = "injection"
FOREGROUND = "foreground"
BACKGROUND = "background"
TRIGGER_TYPES = [INJECTION, FOREGROUND, BACKGROUND]
