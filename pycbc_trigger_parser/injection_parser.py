"""PyCBC Injection Trigger Table module."""
import json
import logging
from typing import Optional

import bilby
import numpy as np
import pandas as pd
from gwpy.table import Table

from .template_bank import TemplateBank
from .trigger_list import TriggerList
from .trigger_table import TriggerTable
from .utils import read_hdf_file

logging.basicConfig(level=logging.INFO)

FAP = "fap"
IFAR = "ifar"
STAT = "stat"
SNR = "snr"
TIMESLIDE_VALUE = "timeslide_value"
INJECTION_PARAMETERS = "injection_parameters"
INJECTION_INDEX = "injection_index"
TEMPLATE_ID = "template_id"

INJECTION_JSON = "injection_parameters.json"


class InjectionTriggerTable(TriggerTable):
    """Store injection trigger info."""

    def __init__(
        self,
        fap: np.ndarray,
        ifar: np.ndarray,
        stat: np.ndarray,
        ifo_trigger: dict,
        template_bank: TemplateBank,
        injection_parameters: pd.DataFrame,
    ):
        """Get injection search data from inj search file and trigger lists.

        :param ifar:
        :param stat:
        :param ifo_trigger:
        :param template_bank:
        """
        self.ifar = ifar
        self.stat = stat
        self.ifo_trigger = ifo_trigger
        self.timeslide_value = np.zeros(len(stat))
        self.snr = self.get_triggers_combined_snr()
        self.template_bank = template_bank
        self.fap = fap
        self.injection_parameters = injection_parameters
        self.dataframe = self.dataframe

    @classmethod
    def from_dataframe(cls, injtrg_table_df: pd.DataFrame):
        """Generate injection trigger from dataframe.

        :param injtrg_table_df:
        :return:
        """
        template_bank = TemplateBank.from_dataframe(injtrg_table_df)
        injection_parameters = injtrg_table_df[get_bilby_injection_column_headers()]
        ifo_trigger = {
            "H1": TriggerList.from_dataframe(injtrg_table_df, "H1"),
            "L1": TriggerList.from_dataframe(injtrg_table_df, "L1"),
        }
        return cls(
            fap=injtrg_table_df[FAP],
            ifar=injtrg_table_df[IFAR],
            stat=injtrg_table_df[STAT],
            template_bank=template_bank,
            injection_parameters=injection_parameters,
            ifo_trigger=ifo_trigger,
        )

    @classmethod
    def from_pycbc_hdf_files(
        cls,
        trigger_table_file: str,
        table_name: str,
        bank_file: str,
        trigger_files: dict,
        injection_file: str,
    ):
        """Generate injection trigger table from pyCBC hdf files.

        :param trigger_table_file:
        :param table_name:
        :param bank_file:
        :param trigger_files:
        :param injection_file:
        :return:
        """
        logging.info("Reading {}".format(trigger_table_file))
        file_data = read_hdf_file(trigger_table_file)
        table_data = file_data.get(table_name)

        # Getting trigger statistics
        fap = table_data.get(FAP)[:]
        ifar = table_data.get(IFAR)[:]
        stat = table_data.get(STAT)[:]

        # Get template bank and triggers
        template_bank = TemplateBank.from_pycbc_hdf_bank_file(
            bank_file=bank_file, id_num=table_data.get(TEMPLATE_ID)[:]
        )
        ifo_trigger = TriggerTable.get_trigger_list(
            file_data, trigger_files, table_data
        )

        injection_index = table_data.get(INJECTION_INDEX)[:]
        injection_parameters = cls.read_injection_parameters_from_xml(
            injection_file, id_number=injection_index
        )

        logging.info("Returning Injection Trigger Table")
        return cls(
            fap=fap,
            ifar=ifar,
            stat=stat,
            template_bank=template_bank,
            ifo_trigger=ifo_trigger,
            injection_parameters=injection_parameters,
        )

    def to_dict(self):
        """Convert injection trigger table to dict.

        :return:
        """
        table_dict = {
            FAP: self.fap,
            IFAR: self.ifar,
            STAT: self.stat,
            SNR: self.snr,
            TIMESLIDE_VALUE: self.timeslide_value,
        }

        for ifo_name, trigger_list in self.ifo_trigger.items():
            table_dict.update(trigger_list.to_dict())

        table_dict.update(self.template_bank.to_dict())
        table_dict.update(self.injection_parameters.to_dict("list"))

        return table_dict

    @staticmethod
    def generate_injection_json(df, json_path=INJECTION_JSON):
        """Generate injection json.

        :param json_path:
        :return:
        """
        injection_df = df[get_bilby_injection_column_headers()]
        injection_df.columns = [
            col.replace("injection_", "") for col in injection_df.columns
        ]
        injections = dict(injections=injection_df)
        with open(json_path, "w") as file:
            json.dump(
                injections, file, indent=2, cls=bilby.core.result.BilbyJsonEncoder
            )
            logging.info(f"Saved injection parameters in {json_path}.")

    @staticmethod
    def read_injection_parameters_from_xml(
        xml_file: str, id_number: Optional[int] = None
    ) -> pd.DataFrame:
        """Open XML and grab parameters from xml.

        :param xml_file: the file with injections
        :param id_number: id number of inj (the row number)
        :return: pd.Dataframe with select injection parameters.
        """
        data = Table.read(xml_file, tablename="sim_inspiral").to_pandas()
        if not isinstance(id_number, type(None)):
            data = data.iloc[id_number]
        data = data.add_prefix("injection_")
        data = data[get_relevent_injection_column_headers()]
        data = convert_to_bilby_aligned_spin_parameters(data)
        return data


def convert_to_bilby_aligned_spin_parameters(df) -> pd.DataFrame:
    """Convert injection parameters to bilby parameters.

    :param df:
    :return:
    """
    bb_df = pd.DataFrame()

    bb_df["injection_mass_1"] = df["injection_mass1"]
    bb_df["injection_mass_2"] = df["injection_mass2"]
    bb_df["injection_geocent_time"] = df["injection_geocent_end_time"] + (
        1e-9 * df["injection_geocent_end_time_ns"]
    )
    bb_df["injection_chi_1"] = abs(df["injection_spin1z"])
    bb_df["injection_chi_2"] = abs(df["injection_spin2z"])

    # bb_df["injection_a_1"] = abs(df["injection_spin1z"])
    # bb_df["injection_a_2"] = abs(df["injection_spin2z"])
    # bb_df["injection_tilt_1"] = np.arccos(np.sign(df["injection_spin1z"]))
    # bb_df["injection_tilt_2"] = np.arccos(np.sign(df["injection_spin2z"]))
    # bb_df["injection_phi_12"] = 0
    # bb_df["injection_phi_jl"] = 0
    bb_df["injection_luminosity_distance"] = df["injection_distance"]
    bb_df["injection_theta_jn"] = df["injection_inclination"]
    bb_df["injection_psi"] = df["injection_polarization"]
    bb_df["injection_phase"] = df["injection_coa_phase"]
    bb_df["injection_ra"] = df["injection_longitude"]
    bb_df["injection_dec"] = df["injection_latitude"]
    return bb_df


def get_bilby_injection_column_headers():
    """Return bilby injection column headers.

    :return:
    """
    return [
        "injection_mass_1",
        "injection_mass_2",
        "injection_geocent_time",
        "injection_chi_1",
        "injection_chi_2",
        # "injection_a_1",
        # "injection_a_2",
        # "injection_tilt_1",
        # "injection_tilt_2",
        # "injection_phi_12",
        # "injection_phi_jl",
        "injection_luminosity_distance",
        "injection_theta_jn",
        "injection_psi",
        "injection_phase",
        "injection_ra",
        "injection_dec",
    ]


def get_relevent_injection_column_headers():
    """Return injection column headers.

    :return:
    """
    return [
        # "injection_alpha",
        # "injection_alpha1",
        # "injection_alpha2",
        # "injection_alpha3",
        # "injection_alpha4",
        # "injection_alpha5",
        # "injection_alpha6",
        # "injection_amp_order",
        # "injection_bandpass",
        # "injection_beta",
        "injection_coa_phase",
        "injection_distance",
        # "injection_eff_dist_g",
        # "injection_eff_dist_h",
        # "injection_eff_dist_l",
        # "injection_eff_dist_t",
        # "injection_eff_dist_v",
        # "injection_end_time_gmst",
        # "injection_eta",
        # "injection_f_final",
        # "injection_f_lower",
        # "injection_g_end_time",
        # "injection_g_end_time_ns",
        "injection_geocent_end_time",
        "injection_geocent_end_time_ns",
        # "injection_h_end_time",
        # "injection_h_end_time_ns",
        "injection_inclination",
        # "injection_l_end_time",
        # "injection_l_end_time_ns",
        "injection_latitude",
        "injection_longitude",
        "injection_mass1",
        "injection_mass2",
        # "injection_mchirp",
        # "injection_numrel_data",
        # "injection_numrel_mode_max",
        # "injection_numrel_mode_min",
        # "injection_phi0",
        "injection_polarization",
        # "injection_process_id",
        # "injection_psi0",
        # "injection_psi3",
        # "injection_simulation_id",
        # "injection_source",
        # "injection_spin1x",
        # "injection_spin1y",
        "injection_spin1z",
        # "injection_spin2x",
        # "injection_spin2y",
        "injection_spin2z",
        # "injection_t_end_time",
        # "injection_t_end_time_ns",
        # "injection_taper",
        # "injection_theta0",
        # "injection_v_end_time",
        # "injection_v_end_time_ns",
        "injection_waveform",
    ]
