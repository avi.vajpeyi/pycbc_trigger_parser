import unittest

from pycbc_trigger_parser import utils


class UtilsTests(unittest.TestCase):
    def setUp(self) -> None:
        self.root = "tests/test_data/trigger_combining/"

    def test_mass_check(self):
        m1, m2 = utils.correct_m1_m2(1, 2)
        self.assertTrue(m1 > m2)
        m1, m2 = utils.correct_m1_m2(2, 1)
        self.assertTrue(m1 > m2)
