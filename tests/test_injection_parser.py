import os
import shutil
import unittest

from pycbc_trigger_parser.injection_parser import InjectionTriggerTable

from . import test_data


class InjectionParserTests(unittest.TestCase):
    def setUp(self):
        self.outdir = test_data.OUT_DIR
        os.makedirs(self.outdir, exist_ok=True)
        self.inj_xml = test_data.INJECTION_XML
        self.inj_json = os.path.join(self.outdir, "inj_param.json")

    def tearDown(self):
        if os.path.isdir(self.outdir):
            shutil.rmtree(self.outdir)

    def test_injection_parsing(self):
        df = InjectionTriggerTable.read_injection_parameters_from_xml(self.inj_xml)
        InjectionTriggerTable.generate_injection_json(df, self.inj_json)
