from __future__ import unicode_literals

import os
import shutil
import unittest

import h5py
import matplotlib
import pandas as pd
from pycbc_trigger_parser import TemplateBank, template_bank
from pycbc_trigger_parser.template_bank import (
    F_LOWER,
    MASS1,
    MASS2,
    SPIN1Z,
    SPIN2Z,
    TEMPLATE_HASH,
)
from tests import test_data

matplotlib.use("agg")


class BankTests(unittest.TestCase):
    def setUp(self) -> None:
        bank_data = test_data.BANK_DATA_10_SAMPLES
        self.template_bank = TemplateBank.from_csv(bank_data)
        self.webdir = os.path.join(test_data.OUT_DIR, "bank_files")
        self.bank_hdf = test_data.BANK_HDF
        self.n = 5
        self.make_h5py_for_testing(
            filename=self.bank_hdf, df=self.template_bank.dataframe.head(self.n)
        )
        os.makedirs(self.webdir, exist_ok=True)

    def tearDown(self):
        if os.path.isdir(self.webdir):
            shutil.rmtree(self.webdir)

    @staticmethod
    def make_h5py_for_testing(filename, df):
        n = len(df)
        f_lower = df[template_bank.F_LOWER].values
        mass_1 = df[template_bank.MASS_1].values
        mass_2 = df[template_bank.MASS_2].values
        spin_1z = df[template_bank.SPIN1Z].values
        spin_2z = df[template_bank.SPIN2Z].values
        template_hash = df[template_bank.TEMPLATE_ID].values

        f = h5py.File(filename, "w")
        f.create_dataset(name=F_LOWER, shape=(n,), dtype="f4", data=f_lower)
        f.create_dataset(name=MASS1, shape=(n,), dtype="f4", data=mass_1)
        f.create_dataset(name=MASS2, shape=(n,), dtype="f4", data=mass_2)
        f.create_dataset(name=SPIN1Z, shape=(n,), dtype="f4", data=spin_1z)
        f.create_dataset(name=SPIN2Z, shape=(n,), dtype="f4", data=spin_2z)
        f.create_dataset(name=TEMPLATE_HASH, shape=(n,), dtype="i8", data=template_hash)
        f.close()

    def test_load_from_hdf(self):
        bank = TemplateBank.from_pycbc_hdf_bank_file(self.bank_hdf)
        TemplateBank.from_pycbc_hdf_bank_file(self.bank_hdf, id_num=3)

        bdict = bank.to_dict()
        for k, v in bdict.items():
            self.assertEqual(self.n, len(v))
        df = bank.dataframe
        self.assertIsInstance(df, pd.DataFrame)

        TemplateBank.from_pycbc_hdf_bank_file(self.bank_hdf, id_num=[1, 2, 3])
