import os

DATA = "tests/test_data"
OUT_DIR = "tests/out"

PRIOR = os.path.join(DATA, "test.prior")
BANK_DATA = os.path.join(DATA, "template_bank.csv")
BANK_HDF = os.path.join(DATA, "hdf_files/template_bank.hdf")
BANK_IMAGE = os.path.join(DATA, "template_bank.csv")
TRIGGER_DATA = os.path.join(DATA, "triggers.csv")

SIZE = 10
TRIGGER_DATA_10_SAMPLES = os.path.join(DATA, "10_triggers.csv")
BANK_DATA_10_SAMPLES = os.path.join(DATA, "10_template_bank.csv")
TRIGGER_EXTRA_DATA_10 = os.path.join(DATA, "10_extra_inf.csv")

INJECTION_XML = os.path.join(DATA, "injection/pycbc_injection_file.xml")
INJECTION_TRIGGERS = os.path.join(DATA, "injection/triggers.csv")
