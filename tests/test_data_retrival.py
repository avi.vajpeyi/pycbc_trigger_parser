import unittest

from pycbc_trigger_parser import pycbc_data


class TriggerTableFilteringTests(unittest.TestCase):
    def test_get_data(self):
        foreground = pycbc_data.get_trigger_data(3, 6, "foreground")
        self.assertEqual(foreground.start_time, 1241725020)
        injections = pycbc_data.get_trigger_data(
            3, 6, "injection", injection_key="BBH", injection_waveform="SEOBNRV4"
        )
        self.assertEqual(injections.start_time, 1241725020)

    def test_get_data_2(self):
        foreground = pycbc_data.get_trigger_data(2, 17, "foreground")
        self.assertIsNotNone(foreground)
        injections = pycbc_data.get_trigger_data(
            2, 17, "injection", injection_key="BBH", injection_waveform="SEOBNRV4"
        )
        self.assertIsNotNone(injections)
        self.assertIsNotNone(injections.injection_file_path)

    def test_get_chunk_number(self):
        with self.assertRaises(ValueError):
            pycbc_data.get_trigger_chunk_number(0)
        self.assertEqual(pycbc_data.get_trigger_chunk_number(1169069154.565), 3)
