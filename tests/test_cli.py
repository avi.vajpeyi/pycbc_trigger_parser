import os
import shutil
from shutil import copyfile
from unittest import TestCase

import mock
import numpy as np
from pycbc_trigger_parser import TemplateBank, TriggerTable, cli
from pycbc_trigger_parser.injection_parser import INJECTION_JSON
from pycbc_trigger_parser.trigger_table_times import GPS_FILE, TIMESLIDE_FILE

from . import test_data


class TestCLI(TestCase):
    def setUp(self):
        self.table = TriggerTable.from_csv(test_data.TRIGGER_DATA_10_SAMPLES)
        self.template_bank = TemplateBank.from_csv(test_data.BANK_DATA_10_SAMPLES)
        self.outdir = test_data.OUT_DIR

    def tearDown(self):
        if os.path.isdir(self.outdir):
            shutil.rmtree(self.outdir)

    @mock.patch("pycbc_trigger_parser.cli.cli_utils.get_cli_input")
    @mock.patch(
        "pycbc_trigger_parser.template_bank.TemplateBank.from_pycbc_hdf_bank_file"
    )
    def test_bank_generation(self, get_bank, argv):
        argv.return_value = [
            "--chunk-number",
            "6",
            "--observing-run",
            "3",
            "--outdir",
            self.outdir,
        ]
        get_bank.return_value = self.template_bank
        cli.main_get_bank()
        self.assertTrue(os.path.isfile(os.path.join(self.outdir, "template_bank.csv")))

    @mock.patch("pycbc_trigger_parser.cli.cli_utils.get_cli_input")
    @mock.patch("pycbc_trigger_parser.trigger_table.TriggerTable.from_pycbc_hdf_files")
    def test_table_generation_and_filtering(self, get_trigger_data, argv):
        obs_run = 3
        chunk_num = 6
        argv.return_value = [
            "--chunk-number",
            str(chunk_num),
            "--observing-run",
            str(obs_run),
            "--outdir",
            self.outdir,
            "--trigger-type",
            "foreground",
            "--injection-type",
            "",
        ]
        get_trigger_data.return_value = self.table
        cli.main_get_raw_triggers()

        # assert that the unfiltered triggers loaded.
        unfiltered_path = os.path.join(self.outdir, "unfiltered_triggers.csv")
        self.assertTrue(os.path.isfile(unfiltered_path))

        argv.return_value = [
            "--unfiltered-csv",
            unfiltered_path,
            "--trigger-type",
            "background",
            "--min-duration",
            "0.00",
            "--max-duration",
            "100",
        ]
        cli.main_filter_triggers()
        self.assertTrue(os.path.isfile(os.path.join(self.outdir, "triggers.csv")))

    @mock.patch("pycbc_trigger_parser.cli.cli_utils.get_cli_input")
    @mock.patch("pycbc_trigger_parser.trigger_table.TriggerTable.from_pycbc_hdf_files")
    def test_table_meta_data(self, get_trigger_data, argv):
        argv.return_value = [
            "--chunk-number",
            "6",
            "--observing-run",
            "3",
            "--outdir",
            self.outdir,
            "--trigger-type",
            "foreground",
            "--injection-type",
            "",
        ]
        get_trigger_data.return_value = self.table
        cli.main_get_raw_triggers()
        unfiltered_path = os.path.join(self.outdir, "unfiltered_triggers.csv")
        self.assertTrue(os.path.isfile(unfiltered_path))

        argv.return_value = [
            "--unfiltered-csv",
            unfiltered_path,
            "--trigger-type",
            "background",
            "--min-duration",
            "0.00",
            "--max-duration",
            "100",
        ]
        cli.main_filter_triggers()
        trigger_csv = os.path.join(self.outdir, "triggers.csv")
        self.assertTrue(os.path.isfile(trigger_csv))

        argv.return_value = [
            "--trigger-csv",
            trigger_csv,
            "--trigger-type",
            "foreground",
        ]
        cli.main_extract_trigger_meta_data()
        self.assertTrue(os.path.isfile(os.path.join(self.outdir, "times.csv")))
        self.assertTrue(os.path.isfile(os.path.join(self.outdir, GPS_FILE)))

        argv.return_value = [
            "--trigger-csv",
            trigger_csv,
            "--trigger-type",
            "background",
        ]
        cli.main_extract_trigger_meta_data()
        self.assertTrue(os.path.isfile(os.path.join(self.outdir, "times.csv")))
        self.assertTrue(os.path.isfile(os.path.join(self.outdir, GPS_FILE)))
        self.assertTrue(os.path.isfile(os.path.join(self.outdir, TIMESLIDE_FILE)))

        shutil.copyfile(test_data.INJECTION_TRIGGERS, dst=trigger_csv)
        argv.return_value = [
            "--trigger-csv",
            trigger_csv,
            "--trigger-type",
            "injection",
        ]
        cli.main_extract_trigger_meta_data()
        self.assertTrue(os.path.isfile(os.path.join(self.outdir, "times.csv")))
        self.assertTrue(os.path.isfile(os.path.join(self.outdir, GPS_FILE)))
        self.assertTrue(os.path.isfile(os.path.join(self.outdir, INJECTION_JSON)))

    @mock.patch("pycbc_trigger_parser.cli.cli_utils.get_cli_input")
    def test_meta_data_with_catalogue_triggers(self, argv):
        trigger_csv = os.path.join(self.outdir, "triggers.csv")
        table = self.table
        table.add_catalogue_events(2, 14)
        new_df = table.dataframe
        table.to_csv(trigger_csv)
        argv.return_value = [
            "--trigger-csv",
            trigger_csv,
            "--trigger-type",
            "foreground",
        ]
        cli.main_extract_trigger_meta_data()
        gps_time_file = os.path.join(self.outdir, GPS_FILE)
        self.assertTrue(os.path.isfile(os.path.join(self.outdir, "times.csv")))
        self.assertTrue(os.path.isfile(gps_time_file))

        self.assertEqual(len(new_df), len(np.loadtxt(gps_time_file)))

    @mock.patch("pycbc_trigger_parser.cli.cli_utils.get_cli_input")
    def test_trigger_plot(self, argv):
        argv.return_value = [
            "--bank-csv",
            f"{test_data.BANK_DATA_10_SAMPLES}",
            "--foreground-csv",
            f"{test_data.TRIGGER_DATA_10_SAMPLES}",
            "--background-csv",
            f"{test_data.TRIGGER_DATA_10_SAMPLES}",
            "--injection-csv",
            f"{test_data.TRIGGER_DATA_10_SAMPLES}",
            "--prior-file",
            "tests/test_data/test.prior",
            "--outdir",
            f"{self.outdir}",
        ]
        cli.main_plot_triggers()
        self.assertTrue(
            os.path.isfile(os.path.join(self.outdir, "template_bank_masses.png")),
            "No trigger plot found",
        )

    @mock.patch("pycbc_trigger_parser.cli.cli_utils.get_cli_input")
    def test_catalogue_trigger_adder(self, argv):
        os.makedirs(self.outdir, exist_ok=True)
        trig_path = os.path.join(self.outdir, "temp_triggers.csv")
        copyfile(test_data.TRIGGER_DATA_10_SAMPLES, trig_path)
        argv.return_value = [
            "--trigger-csv",
            f"{trig_path}",
            "--chunk-number",
            "14",
            "--observing-run",
            "2",
        ]
        cli.main_add_catalogue_triggers()

        table = TriggerTable.from_csv(trig_path)
        self.assertTrue(len(table.dataframe) == 13)
