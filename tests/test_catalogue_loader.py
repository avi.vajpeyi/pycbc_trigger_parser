import unittest

import pandas as pd
from pycbc_trigger_parser.catalogue_events import CatalogueEvents


class TestCatalogueEventLoader(unittest.TestCase):
    def test_loader(self):
        c = CatalogueEvents()
        self.assertIsInstance(c.data, pd.DataFrame)
        self.assertFalse(c.to_trigger_table_df().empty)
        df = c.data

    def test_specific_getter(self):
        self.assertFalse(CatalogueEvents.get_ias_events().data.empty)
        self.assertFalse(CatalogueEvents.get_gwtc_events().data.empty)
        self.assertFalse(CatalogueEvents.get_chunk_events(2, 14).data.empty)


if __name__ == "__main__":
    unittest.main()
