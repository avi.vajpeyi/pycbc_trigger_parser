import os
import shutil
import unittest

from pandas.util.testing import assert_frame_equal
from pycbc_trigger_parser.trigger_table import TriggerTable
from tests import test_data


class TriggerTableTests(unittest.TestCase):
    def setUp(self) -> None:
        self.trigger_csv = test_data.TRIGGER_DATA_10_SAMPLES
        self.table = TriggerTable.from_csv(self.trigger_csv)
        self.trigger_csv_new = os.path.join(test_data.OUT_DIR, "10_triggers.csv")

    def tearDown(self) -> None:
        if os.path.exists(test_data.OUT_DIR) and os.path.isdir(test_data.OUT_DIR):
            shutil.rmtree(test_data.OUT_DIR)

    def test_to_csv(self):
        self.table.to_csv(self.trigger_csv_new)
        temp_file_in_cwd = "here.csv"
        self.table.to_csv(temp_file_in_cwd)
        os.remove(temp_file_in_cwd)

    def test_trigger_combined_snr(self):
        self.assertEqual(self.table.snr.shape, (test_data.SIZE,))

    def test_saving_one_trigger(self):
        filtered_table = self.table.get_filtered_table(
            template_id_min=334026, template_id_max=334026
        )
        self.assertTrue(len(filtered_table.dataframe) == 1)
        temp_file_in_cwd = "here.csv"
        filtered_table.to_csv(temp_file_in_cwd)
        self.assertTrue(os.path.isfile(temp_file_in_cwd))
        os.remove(temp_file_in_cwd)

    def test_trigger_table_add_catalogue_events(self):
        # load a trigger table and only keep 1 trigger
        filtered_table = self.table.get_filtered_table(
            template_id_min=334026, template_id_max=334026
        )
        self.assertTrue(len(filtered_table.dataframe) == 1)

        # add catalogue triggers
        filtered_table.add_catalogue_events(2, 14)
        df_with_catalogue = filtered_table.dataframe
        self.assertTrue(len(df_with_catalogue) > 1)

        # store the new trigger table (with catalogue trigger)
        temp_file = os.path.join(test_data.OUT_DIR, "temp_file.csv")
        filtered_table.to_csv(temp_file)

        # check if loading the trigger table (with catalogue trigger) possible
        trigger_table_df = TriggerTable.from_csv(temp_file).dataframe
        df_with_catalogue.index = trigger_table_df.index  # don't care if idx different
        dif_cols = list(
            set(trigger_table_df.columns.values) - set(df_with_catalogue.columns.values)
        )
        self.assertTrue(len(dif_cols) == 0, msg=str(dif_cols))
        assert_frame_equal(
            trigger_table_df,
            df_with_catalogue,
            check_less_precise=True,
            check_like=True,
        )


if __name__ == "__main__":
    unittest.main()
