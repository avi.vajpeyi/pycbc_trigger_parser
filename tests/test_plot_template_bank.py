from __future__ import unicode_literals

import os
import shutil
import unittest

import matplotlib
import numpy as np
from pycbc_trigger_parser import TemplateBank, TriggerTable
from pycbc_trigger_parser.plotting import (
    plot_interactive_template_bank,
    plot_template_bank,
)
from tests import test_data

matplotlib.use("agg")


class BankPlottingTests(unittest.TestCase):
    def setUp(self) -> None:
        bank_data = test_data.BANK_DATA_10_SAMPLES
        trigger_data = test_data.TRIGGER_DATA_10_SAMPLES

        self.template_bank = TemplateBank.from_csv(bank_data)
        self.triggers = TriggerTable.from_csv(trigger_data)

        self.webdir = os.path.join(test_data.OUT_DIR, "bank_files")
        os.makedirs(self.webdir, exist_ok=True)

    # def tearDown(self) -> None:
    #     if os.path.exists(test_data.OUT_DIR) and os.path.isdir(test_data.OUT_DIR):
    #         shutil.rmtree(test_data.OUT_DIR)

    def get_plotting_data(self):
        scatter_points = [
            # TEMPLATE BANK
            dict(
                data=self.template_bank.dataframe,
                plot_kwargs=dict(
                    c="r", s=0.5, marker=".", alpha=0.5, label="Template Bank"
                ),
            ),
            # ALL BACKGROUND TRIGGERS
            dict(
                data=self.triggers.dataframe,
                plot_kwargs=dict(c="k", s=5, marker="o", label="Template Bank"),
            ),
        ]
        countour_lines = [
            dict(
                contour_condition=my_criteria,
                plot_kwargs=dict(
                    colors="b", linestyles="--", linewidths=2.0, label="Prior"
                ),
            ),
            # dict(
            #     contour_condition=my_criteria_2,
            #     plot_kwargs=dict(
            #         colors="b", linestyles="--", linewidths=2.0, label="Prior"
            #     ),
            # ),
        ]

        return scatter_points, countour_lines

    def test_template_bank_plot(self):
        scatter_points, countour_lines = self.get_plotting_data()

        kwargs = dict(
            scatter_points=scatter_points,
            countour_lines=countour_lines,
            web_dir=self.webdir,
        )

        fig, ax_m1m2, ax_mcq, legend_handles = plot_template_bank(**kwargs)

        m1, m2 = (self.triggers.dataframe.mass_1, self.triggers.dataframe.mass_2)
        x, y = np.meshgrid(m1, m2)
        z = np.ones(shape=(len(m1), len(m2)))
        ax_m1m2.contour(x, y, z, [0], colors="k", linestyles="--", linewidths=2.0)
        fig.savefig(os.path.join(self.webdir, "template_bank_masses_with_event.png"))

    def test_plot_interactive(self):
        plot_interactive_template_bank(scatter_list=[])


def my_criteria(m1: float, m2: float, mc: float, q: float, M: float) -> int:
    """
    :param m1: mass1 val
    :param m2: mass2 val
    :param mc: chirp mass val
    :param q: mass ratio val
    :param M: total mass val
    :return: 1 if above parameters inside criteria (defined in function), otherwise 0
    """
    if (
        30 <= m1 <= 250
        and 30 <= m2 <= 250
        and 70 <= mc <= 150
        and 0.08 <= q <= 1
        and 200 <= M <= 340
    ):
        return 1
    else:
        return 0


def my_criteria_2(m1: float, m2: float, mc: float, q: float, M: float) -> int:
    """
    :param m1: mass1 val
    :param m2: mass2 val
    :param mc: chirp mass val
    :param q: mass ratio val
    :param M: total mass val
    :return: 1 if above parameters inside criteria (defined in function), otherwise 0
    """
    if 7 <= mc <= 150 and 0.01 <= q <= 1 and 50 <= M <= 500:
        return 1
    else:
        return 0


# def get_trigger_times_from_gracedb(my_trigger_time: int) -> List:
#     """
#     returns a list of grace db gps trigger times +/- 5s
#     from the provided trigger time
#
#     :param my_trigger_time: a GPS time provided by the user
#     :return: list of gps trigger times
#     """
#
#     search_time_start = my_trigger_time - 5
#     end_time_start = my_trigger_time + 5
#
#     # NEED FOLLOWING FUNCTION
#     gracedb_gps_times = gracedb_trigger_times(search_time_start, end_time_start)
#
#     return gracedb_gps_times
