import os
import unittest

import pandas as pd
from pandas.testing import assert_frame_equal
from pycbc_trigger_parser import TemplateBank
from pycbc_trigger_parser.trigger_table import TriggerTable
from tests import test_data


class TriggerTableFilteringTests(unittest.TestCase):
    def setUp(self) -> None:
        self.table = TriggerTable.from_csv(test_data.TRIGGER_DATA_10_SAMPLES)
        self.bank = TemplateBank.from_csv(test_data.BANK_DATA_10_SAMPLES)

    def test_filtering_no_criteria_provided(self):
        filtered_table = self.table.get_filtered_table()
        self.assertIsNot(filtered_table, self.table)
        assert_frame_equal(filtered_table.dataframe, self.table.dataframe)

    def test_filtering_some_criteria_provided(self):
        filtered_table = self.table.get_filtered_table(
            snr_min=7, snr_max=9, mass_total_min=5
        )
        self.assertTrue(len(self.table.dataframe) > len(filtered_table.dataframe))

    def test_invalid_filtering_criteria_provided(self):
        self.assertRaises(
            pd.core.computation.ops.UndefinedVariableError,
            raises_undefined_variable_error_error,
        )
        self.assertRaises(ValueError, raises_val_error)

    def test_filtering_with_prior(self):
        import bilby

        prior_file = test_data.PRIOR
        prior = bilby.prior.PriorDict(filename=prior_file)
        filtered_trigger_table = self.table.get_filtered_table(
            mass_chirp_min=prior["chirp_mass"].minimum,
            mass_chirp_max=prior["chirp_mass"].maximum,
            mass_total_min=prior["total_mass"].minimum,
            mass_total_max=prior["total_mass"].maximum,
            mass_ratio_min=prior["mass_ratio"].minimum,
            mass_ratio_max=prior["mass_ratio"].maximum,
            mass_1_min=prior["mass_1"].minimum,
            mass_1_max=prior["mass_1"].maximum,
            mass_2_min=prior["mass_2"].minimum,
            mass_2_max=prior["mass_2"].maximum,
            H1_template_duration_max=4,
            L1_template_duration_max=4,
        )

        self.assertNotEqual(
            filtered_trigger_table.dataframe.to_dict(), self.table.dataframe.to_dict()
        )

    def test_filtering_to_one_trigger(self):
        table = TriggerTable.from_csv(
            "tests/test_data/filtering_test_files/unfiltered_triggers.csv"
        )
        filtered_trigger_table = table.get_filtered_table(
            H1_template_duration_max=0.454,
            L1_template_duration_max=0.454,
            H1_template_duration_min=0,
            L1_template_duration_min=0,
        )
        temp_file = "temp.csv"
        filtered_trigger_table.to_csv(temp_file)
        self.assertTrue(os.path.isfile(temp_file))
        os.remove(temp_file)


def raises_undefined_variable_error_error():
    table = TriggerTable.from_csv(test_data.TRIGGER_DATA_10_SAMPLES)
    table.get_filtered_table(nonesense_min=7)


def raises_val_error():
    table = TriggerTable.from_csv(test_data.TRIGGER_DATA_10_SAMPLES)
    table.get_filtered_table(snr=7)


if __name__ == "__main__":
    unittest.main()
