import os
import unittest

import pandas as pd
from pycbc_trigger_parser import utils

EVID = "evidences_{}.csv"
TIME = "times_{}.csv"
TRIG = "triggers_{}.csv"
GPS_TIME = "gps_time"


class CombineTests(unittest.TestCase):
    def setUp(self) -> None:
        self.root = "tests/test_data/trigger_combining/"
        self.evid_f = os.path.join(self.root, EVID)
        self.time_f = os.path.join(self.root, TIME)
        self.trig_f = os.path.join(self.root, TRIG)

    def get_data(self, num):
        evid_df = pd.read_csv(self.evid_f.format(num))
        time_df = pd.read_csv(self.time_f.format(num))
        trigger_df = pd.read_csv(self.trig_f.format(num))
        evid_df = evid_df.sort_values(by=GPS_TIME)
        return evid_df, time_df, trigger_df

    def check_difference_in_combined_dataframe(self, combined_df, evid_df):
        combined_len = len(combined_df)
        evid_len = len(evid_df)
        self.assertTrue(
            combined_len == evid_len,
            f"{combined_len} evidences stored from a total of {evid_len} evidences",
        )

    def test_combine(self):
        """Test the combining of CSVs based on the GPS start time."""
        evid_df, time_df, trigger_df = self.get_data(0)
        combined_df, unpaired_df = utils.combine_trigger_and_extra_data_using_gpstime(
            other_df=evid_df, trigger_df=trigger_df, time_df=time_df
        )
        self.check_difference_in_combined_dataframe(combined_df, evid_df)

    def test_combine1(self):
        evid_df, time_df, trigger_df = self.get_data(1)
        evid_df[GPS_TIME] = evid_df[GPS_TIME] + 2
        combined_df, unpaired_df = utils.combine_trigger_and_extra_data_using_gpstime(
            other_df=evid_df, trigger_df=trigger_df, time_df=time_df
        )
        self.check_difference_in_combined_dataframe(combined_df, evid_df)

    def test_combine2(self):
        evid_df, time_df, trigger_df = self.get_data(2)
        combined_df, unpaired_df = utils.combine_trigger_and_extra_data_using_gpstime(
            other_df=evid_df, trigger_df=trigger_df, time_df=time_df
        )
        self.check_difference_in_combined_dataframe(combined_df, evid_df)

    def test_combine_time_and_trigger_empty(self):
        evid_df, time_df, trigger_df = self.get_data(3)
        combined_df, unpaired_df = utils.combine_trigger_and_extra_data_using_gpstime(
            other_df=evid_df, trigger_df=trigger_df, time_df=time_df
        )
        self.check_difference_in_combined_dataframe(combined_df, evid_df)


if __name__ == "__main__":
    unittest.main()
