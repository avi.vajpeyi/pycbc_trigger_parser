import os
import shutil
import unittest

import numpy as np
import pandas as pd
from pycbc_trigger_parser import utils
from pycbc_trigger_parser.trigger_table import TriggerTable
from pycbc_trigger_parser.trigger_table_times import TriggerTableTimes
from tests import test_data

START_TIME = 1241725020
END_TIME = 1242485126


class TriggerTableTimeTests(unittest.TestCase):
    def setUp(self) -> None:
        self.prior = test_data.PRIOR
        self.bank_image = test_data.BANK_IMAGE
        self.trigger_csv = test_data.TRIGGER_DATA_10_SAMPLES
        self.webdir = os.path.join(test_data.OUT_DIR, "times_webdir")

        self.times = os.path.join(test_data.OUT_DIR, "times.csv")
        self.table = TriggerTable.from_csv(self.trigger_csv)
        self.table_times = TriggerTableTimes(self.table)

    def tearDown(self) -> None:
        if os.path.exists(test_data.OUT_DIR) and os.path.isdir(test_data.OUT_DIR):
            shutil.rmtree(test_data.OUT_DIR)

    def test_get_timeslid_start_times(self):
        timeslid_starts = self.table_times.timeslid_start_times
        self.assertIsInstance(timeslid_starts, dict)
        for times in timeslid_starts.values():
            self.assertEqual(times.shape, (test_data.SIZE,))

    def test_get_avg_timeslid_start_time(self):
        avg_timeslid_start_time = self.table_times.average_timeslid_start_time
        self.assertEqual(avg_timeslid_start_time.shape, (test_data.SIZE,))

    def test_to_txt(self):
        # test saving to file
        table_times = self.table_times
        table_times.save_time_txt_files(txt_dir=self.webdir, save_timeslides=False)

        # create variables with names of files
        start_time_fname = os.path.join(self.webdir, "gps_start_time.txt")
        timeslide_fname = os.path.join(self.webdir, "timeslide.txt")

        # check that a timeslide file is not created
        self.assertTrue(os.path.isfile(start_time_fname))
        self.assertFalse(os.path.isfile(timeslide_fname))

        # create timeslide file and gps time file
        table_times.save_time_txt_files(txt_dir=self.webdir, save_timeslides=True)
        self.assertTrue(os.path.isfile(timeslide_fname))

        # test reading avg start time data from file
        loaded_avg_start_times = np.loadtxt(start_time_fname)
        loaded_timeslides = np.loadtxt(timeslide_fname)
        self.assertEqual(loaded_avg_start_times.shape, (test_data.SIZE,))
        self.assertEqual(loaded_timeslides.shape, (test_data.SIZE, 2))

        # assert reconstructed timeslid times
        timeslid_h1_time_loaded = loaded_avg_start_times + loaded_timeslides[:, 0]
        timeslid_l1_time_loaded = loaded_avg_start_times + loaded_timeslides[:, 1]
        self.assertEqual(timeslid_h1_time_loaded.shape, (test_data.SIZE,))
        self.assertEqual(timeslid_l1_time_loaded.shape, (test_data.SIZE,))

        # testing reconstructed timeslid time with original timeslid time differences
        h1_time = self.table.ifo_trigger["H1"].start_time
        l1_time = self.table.ifo_trigger["L1"].start_time
        timeshift = self.table.timeslide_value
        self.assertIsNotNone(timeshift)

        timeslid_h1_time = h1_time  # + timeshift
        timeslid_l1_time = l1_time  # + timeshift
        residual_h1 = timeslid_h1_time - timeslid_h1_time_loaded
        residual_l1 = timeslid_l1_time - timeslid_l1_time_loaded
        for i in range(0, len(timeslid_h1_time)):
            self.assertAlmostEqual(residual_h1.iloc[i], 0, delta=0.0001, msg=f"i={i}")
            self.assertAlmostEqual(residual_l1.iloc[i], 0, delta=0.0001, msg=f"i={i}")

        table_times.to_csv(self.times)

    def test_combine_df(self):
        extra_df = pd.read_csv(test_data.TRIGGER_EXTRA_DATA_10)
        combined_df, unpaired_rows = utils.combine_trigger_and_extra_data_using_gpstime(
            trigger_df=self.table.dataframe,
            time_df=self.table_times.to_dataframe(),
            other_df=extra_df,
        )
        # from pandas.util.testing import assert_frame_equal
        #
        # df_to_compare = pd.read_csv("tests/test_data/10_combined.csv")
        # assert_frame_equal(combined_df, df_to_compare)
        # print(combined_df)
        self.assertEqual(10, len(combined_df))


if __name__ == "__main__":
    unittest.main()
