import os
import shutil
import unittest

from pycbc_trigger_parser.trigger_table_webpage import TriggerPage
from tests import test_data

START_TIME = 1241725020
END_TIME = 1242485126

SIZE = 10


class TriggerPageTests(unittest.TestCase):
    def setUp(self) -> None:
        self.trigger_csv = test_data.TRIGGER_DATA_10_SAMPLES
        self.webdir = os.path.join(test_data.OUT_DIR, "webdir")

    def tearDown(self) -> None:
        if os.path.exists(test_data.OUT_DIR) and os.path.isdir(test_data.OUT_DIR):
            shutil.rmtree(test_data.OUT_DIR)

    def test_trigger_page(self):
        trigger_page = TriggerPage(
            webdir=self.webdir,
            trigger_csv=self.trigger_csv,
            title="O3 Chunk6 Triggers",
            bank_file_path=test_data.BANK_IMAGE,
        )
        trigger_page.render()


if __name__ == "__main__":
    unittest.main()
